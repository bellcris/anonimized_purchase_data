\select@language {english}
\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {subparagraph}{Important purchases}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}Handling and storing individual personal data}{3}{subsection.1.1}
\contentsline {subparagraph}{Transmitting personal data}{3}{subsection.1.1}
\contentsline {subparagraph}{Storing personal data}{3}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Usefulness and necessity of personal data}{4}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Study approach}{4}{subsection.1.3}
\contentsline {section}{\numberline {2}Related work}{6}{section.2}
\contentsline {subsection}{\numberline {2.1}Preserving personal information privacy with k-Anonymity}{6}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Differential privacy}{7}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Online privacy and security effects on consumer behaviour}{7}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Loyalty programs effect on consumer behaviour}{8}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Privacy enabled loyalty cards}{9}{subsection.2.5}
\contentsline {section}{\numberline {3}System design}{11}{section.3}
\contentsline {subsection}{\numberline {3.1}Hardware, programming languages, tools and frameworks used}{11}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Generating a data set with purchases}{12}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Anonymizing data}{15}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Mining association rules}{19}{subsection.3.4}
\contentsline {subparagraph}{Items}{19}{subsection.3.4}
\contentsline {subparagraph}{Transaction}{19}{subsection.3.4}
\contentsline {subparagraph}{Itemset}{19}{subsection.3.4}
\contentsline {subparagraph}{Rule}{19}{subsection.3.4}
\contentsline {subparagraph}{Minimum support}{20}{subsection.3.4}
\contentsline {subparagraph}{Confidence}{20}{subsection.3.4}
\contentsline {subparagraph}{Lift}{20}{subsection.3.4}
\contentsline {subparagraph}{Conviction}{20}{subsection.3.4}
\contentsline {subparagraph}{Mining methods}{20}{subsection.3.4}
\contentsline {section}{\numberline {4}Evaluation and Findings}{27}{section.4}
\contentsline {subsection}{\numberline {4.1}Preparing for the first run, data set and system parameters}{27}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}First run and discoveries made}{32}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Findings for the naive implementation of k-anonymity}{33}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Findings for the similarity-based grouping implementation of k-anonymity}{35}{subsection.4.4}
\contentsline {section}{\numberline {5}Conclusions and future work}{39}{section.5}
\contentsline {subsection}{\numberline {5.1}Important Achievements}{39}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Future work}{39}{subsection.5.2}
\contentsline {section}{References}{41}{subsection.5.2}
\contentsline {section}{\numberline {A}Appendix}{45}{appendix.A}
\contentsline {section}{\numberline {A}CD contents}{49}{appendix.A}
\contentsline {subsection}{\numberline {A.1}./paper folder}{49}{subsection.A.1}
\contentsline {subsection}{\numberline {A.2}./source\_code folder}{49}{subsection.A.2}
\contentsline {subsection}{\numberline {A.3}./results folder}{49}{subsection.A.3}
