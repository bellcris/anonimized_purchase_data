from core import assoc_rules_finder
from core.assoc_rules_finder import min_support_default


def menu():
    # a if condition else b
    min_support_in = input("\nplease input a min_support [in % or 1 to 10] OR x to exit: ")
    min_support = min_support_default
    try:
        min_support_in = int(min_support_in)
        if min_support_in is not None and 0 < min_support_in <= 100:
            min_support = min_support_in
        '''elif min_support_in is not None and 10 < min_support_in <= 100:
            min_support = (min_support_in/10)'''
        # confidence = input("please provide confidence: ")
    except ValueError:
        if min_support_in == 'x':
            exit()
        print('Invalid number entered, using default min_support: {}'.format(min_support_default))

    print_minimal = input("minimal tree print [y/n] (def:n): ")
    print_minimal = str(print_minimal)
    if print_minimal is 'y' or print_minimal is 'Y':
        print_minimal = True
    else:
        print_minimal = False

    assoc_rules_finder.filter_items(min_support)  # remove items with support < min_supp

    # action = input("find association (r)ules or e(x)it: ")
    action = 'r'

    if action == 'r':
        assoc_rules_finder.find_assoc_rules(print_minimal)

    elif action == 'x':
        exit()
    menu()


assoc_rules_finder.start(True)
menu()
