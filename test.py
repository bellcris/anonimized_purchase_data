from multiprocessing import Process, Queue
from multiprocessing.managers import BaseManager


class Worker(Process):
    def __init__(self, q):
        self.q = q
        super(Worker, self).__init__()

    def run(self):
        self.q.put('local hello')


queue = Queue()
w = Worker(queue)
w.start()


class QueueManager(BaseManager):
    pass


def add_to_q():
    for item in dic:
        add_to_q_r(item)


def add_to_q_r(item):
    queue.put(item)
    QueueManager.register('get_queue', callable=lambda: queue)
    m = QueueManager(address=('', 50000), authkey=b'abc')
    s = m.get_server()
    s.serve_forever()


dic = [9, 2, 2, 34, 54, 7, 4, 6]

add_to_q()


'''
from core.util import Util

util = Util()

a_dict = util.fp_powerset([5, 6, 3, 9])
for item in a_dict:
    print(item)

'''
