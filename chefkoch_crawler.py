# coding=utf-8
from BeautifulSoup import BeautifulSoup
import urllib2
import sqlite3 as lite
import HTMLParser

con = lite.connect('chefkoch.db')
con.text_factory = str

known_recipes = []

number_of_recipes = 0
target_number = 1000

current_url = "http://www.chefkoch.de/rezepte/zufallsrezept/?t=1461312011"
chefkoch_prefix = "http://www.chefkoch.de"

with con:

    con.row_factory = lite.Row

    cur = con.cursor()

    # to avoid compatibility issues with previous versions, we need to build the db new from the ground up,
    # if it still has the old schema
    new_version = cur.execute("select name from sqlite_master WHERE type='table' AND name='ingredient_names'").fetchone()

    if not new_version:
        print("Database needs to be built new.")

        cur.execute("drop table if exists recipe_names")
        cur.execute("drop table if exists recipe_ingredients")

    # ggF entsprechende tables erzeugen

    cur.execute("create table if not exists recipe_names "
                "(recipe_id INTEGER PRIMARY KEY, "
                "recipe_name TEXT NOT NULL)")
    # con.commit()

    cur.execute("create table if not exists ingredient_names "
                "(ingredient_id INTEGER PRIMARY KEY AUTOINCREMENT,"
                "ingredient_name TEXT)")

    cur.execute("create table if not exists recipe_ingredients "
                "(recipe_id INTEGER,"
                "ingredient_id INTEGER,"
                "FOREIGN KEY(recipe_id) REFERENCES recipe_names(recipe_id),"
                "FOREIGN KEY(ingredient_id) REFERENCES ingredient_names(ingredient_id))")

    cur.execute("SELECT DISTINCT recipe_id FROM recipe_names")

    results = cur.fetchall()

    for recipe in results:
        known_recipes.append(recipe[0])

    number_of_recipes += len(known_recipes)

    while number_of_recipes < target_number:

        page = urllib2.urlopen(current_url)
        soup = BeautifulSoup(page.read())

        link = soup.find('link', {'rel': 'canonical'})
        link = unicode(link)
        link = link.split('href="')[1]
        link = link.split('"')[0]

        print(link)

        link = link.replace('https', 'http')  # some urls have https instead of http
        recipe_id = link.split('http://www.chefkoch.de/rezepte/')[1]
        recipe_id = recipe_id.split("/")[0]

        # skip if already known
        if int(recipe_id) in known_recipes:
            continue

        recipe_name = soup.find('div', {'class': 'content'})

        if recipe_name:

            recipe_name = unicode(recipe_name)
            recipe_name = recipe_name.split(u'<h1 class="page-title">')[1]
            recipe_name = recipe_name.split(u'</h1>')[0]
            recipe_name = HTMLParser.HTMLParser().unescape(recipe_name)

            print(unicode(recipe_name))

        ingredients = soup.find('div', {'id': 'recipe-incredients'})

        if not ingredients:
            # ggF fehlermeldung
            print("no ingredients found for " + link)
            continue

        # all seems ok
        # add recipe to db
        known_recipes.append(int(recipe_id))

        cur.execute("Insert into recipe_names (recipe_id, recipe_name) Values (?, ?)",
                    (int(recipe_id), recipe_name))

        for ingredient in ingredients.findAll("td"):

            # skip amounts
            if str(ingredient).count('class="amount"') > 0:
                continue

            ingredient_name = ingredient.getText().split()[0].split('(')[0].split(',')[0]

            # check if ingredient already existing
            existing = cur.execute("SELECT * FROM ingredient_names WHERE ingredient_name=?",
                                   (ingredient_name,)).fetchone()

            if existing:
                # we already have that ingredient present
                print(ingredient_name + " (already present)")
                ingredient_id = existing[0]
            else:
                # insert ingredient into db
                print(ingredient_name)
                cur.execute("Insert into ingredient_names (ingredient_name) Values (?)", (ingredient_name,))
                ingredient_id = cur.lastrowid

            cur.execute("Insert into recipe_ingredients (recipe_id, ingredient_id) Values (?, ?)",
                        (int(recipe_id), int(ingredient_id)))

        con.commit()

        number_of_recipes += 1

