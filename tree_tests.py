from core.node import Node
from core.fp_tree import FPTree


def test_add_many_to_tree():
    names = ((1, "Jose", 2), (2, "Rolf", 1), (3, "Anna", 3))

    nodes = [Node(nr, name, counter) for nr, name, counter in names]

    tree = FPTree()

    for node in nodes:
        tree.add(node)

    tree.print_inorder()
    # tree.print_root()


test_add_many_to_tree()
