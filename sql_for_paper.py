import os.path
import sqlite3 as lite
from random import randrange

path = os.path.join(os.path.abspath('.'), __file__)
path_cwd = os.getcwd()
prefix_path_sql = './data/'
tbl_purchases = 'purchases_articles'
if 'core' in path:
    from util import BColors
    from util_func import print_progress
else:
    from core.util import BColors
    from core.util_func import print_progress


if 'core' in path_cwd:
    prefix_path_sql = '../' + prefix_path_sql


def runme(number=None):

    sql_file = 'chefkoch.w.data.db'
    if number is not None:
        sql_file = 'chefkoch.w.data' + str(number) + '.db'

    con = lite.connect(prefix_path_sql + sql_file)
    con.text_factory = str

    with con:
        cur = con.cursor()
        new_version = cur.execute("SELECT name FROM sqlite_master WHERE type='table' AND "
                                  "name='" + tbl_purchases + "'").fetchone()

        if not new_version:
            print("There are no tables in the database, please run the crawler script")
            exit(0)

        '''sql_clean = 'DELETE FROM ' + tbl_purchases + ' WHERE article_id > 1858';
        cur.execute(sql_clean)
        pass
        '''

        # top 15%:
        sql_tot = 'SELECT COUNT(article_id) FROM ' + tbl_purchases
        total_purchased = cur.execute(sql_tot).fetchone()
        total_purchased = int(total_purchased[0])

        sql_tot = 'SELECT COUNT(nr) FROM (SELECT DISTINCT(article_id) AS nr FROM ' + tbl_purchases + ')'
        total_u_purchased = cur.execute(sql_tot).fetchone()
        total_u_purchased = int(total_u_purchased[0])
        print('total distinct items purchased: {}'.format(total_u_purchased))

        sql_top15 = 'SELECT article_id, COUNT(article_id) as nr FROM ' + tbl_purchases + ' GROUP BY article_id ' \
                    'ORDER BY nr DESC LIMIT {}'.format(round(0.15*total_u_purchased))

        '''
        cur.execute(sql_top15)
        total = 0
        for article in cur.fetchall():
            total += int(article[1])
            print('purchased #: {} for id: {}'.format(article[1], article[0]))
        print('top 15% IDs: {}, meaning {}'.format(total, round(total/total_purchased, 2)))
'''

        sql_purchases_per_article = 'SELECT article_id, COUNT(article_id) FROM ' + tbl_purchases + ' GROUP BY article_id'
        cur.execute(sql_purchases_per_article)
        total = 0
        with open('count_' + str(number) + '.csv', 'w+') as f:
            for purchse_p_article in cur.fetchall():
                # print('{}'.format(purchse_p_article))
                print('art_id: {}, COUNT(): {}'.format(purchse_p_article[0], purchse_p_article[1]))
                f.write("%s\n" % '{}, {}'.format(purchse_p_article[0], purchse_p_article[1]))
    con.close()


initial = 0
while initial < 20:
    runme(initial)
    initial += 1


def to_file(transactions, f_name):
    with open(f_name, 'w+') as f:
        for item in transactions:
            f.write("%s\n" % '{}. {}'.format(item, transactions[item]))


def results_to_json(f_name, ar, L, nr_transactions, nr_in_base=None, nr_base=None):
    import json
    import zipfile
    if not os.path.exists(results_dir):
        os.makedirs(results_dir)
    with open(results_dir + f_name, 'w+') as f:
        f.write("%s" % '{\n')
        f.write("%s\n" % '"nr_transact_min_supp":{}, '.format(nr_transactions))
        if nr_in_base:
            f.write("%s\n" % '"in_baseline_rules":{}, '.format(nr_in_base))
        if nr_base:
            f.write("%s\n" % '"from_baseline_rules":{}, '.format(nr_base))
        f.write("%s" % '"L":')
        json.dump(L, f)
        f.write("%s" % ',\n"ar":')
        json.dump(ar, f)
        f.write("\n%s" % '}')

    if compress_results:
        try:
            import zlib
            compression = zipfile.ZIP_DEFLATED
        except (ImportError, ModuleNotFoundError) as e:
            compression = zipfile.ZIP_STORED

        if os.path.isfile(results_dir + f_name):
            with zipfile.ZipFile(results_dir + f_name + '.zip', 'w') as myzip:
                myzip.write(results_dir + f_name, compress_type=compression)