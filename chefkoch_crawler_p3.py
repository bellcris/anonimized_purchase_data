# coding=utf-8
'''
this is the chefkoch_crawler for Python3
for BeautifulSoup see info here: https://www.crummy.com/software/BeautifulSoup/bs4/doc/#installing-beautiful-soup
'''
from bs4 import BeautifulSoup
from urllib.request import urlopen
import sqlite3 as lite

con = lite.connect('chefkoch.db')
con.text_factory = str

known_recipes = []

number_of_recipes = 0
target_number = 10000
unwanted_words = ['Zum', 'Außerdem:', 'Dr.', 'Variante:', 'Für']

current_url = "http://www.chefkoch.de/rezepte/zufallsrezept/?t=1461312011"
chefkoch_prefix = "http://www.chefkoch.de"

with con:

    con.row_factory = lite.Row

    cur = con.cursor()

    # if there is an old schema we will build the db from the ground up. This way any compatibility issues with
    # previous versions are avoided
    new_version = cur.execute("select name from sqlite_master WHERE type='table' AND "
                              "name='ingredient_names'").fetchone()

    if not new_version:
        print("Database needs to be built new.")

        cur.execute("DROP table IF EXISTS recipe_names")
        cur.execute("DROP table IF EXISTS recipe_ingredients")

    # also create the required tables

    cur.execute("CREATE table IF NOT EXISTS recipe_names "
                "(recipe_id INTEGER PRIMARY KEY, "
                "recipe_name TEXT NOT NULL)")
    # con.commit()

    cur.execute("CREATE table IF NOT EXISTS ingredient_names "
                "(ingredient_id INTEGER PRIMARY KEY AUTOINCREMENT,"
                "ingredient_name TEXT)")

    cur.execute("CREATE table IF NOT EXISTS recipe_ingredients "
                "(recipe_id INTEGER,"
                "ingredient_id INTEGER,"
                "FOREIGN KEY(recipe_id) REFERENCES recipe_names(recipe_id),"
                "FOREIGN KEY(ingredient_id) REFERENCES ingredient_names(ingredient_id))")

    cur.execute("SELECT DISTINCT recipe_id FROM recipe_names")

    results = cur.fetchall()

    for recipe in results:
        known_recipes.append(recipe[0])

    number_of_recipes += len(known_recipes)

    a = 0
    while number_of_recipes < target_number:
        a = a + 1
        if a % 25 is 0:
            print("                                          Progress {}/{}".format(len(known_recipes), target_number))

        page = urlopen(current_url)
        soup = BeautifulSoup(page.read(), "lxml")

        link = soup.find('link', {'rel': 'canonical'})
        link = str(link)
        link = link.split('href="')[1]
        link = link.split('"')[0]

        link = link.replace('https', 'http')  # some urls have https instead of http
        recipe_id = link.split('http://www.chefkoch.de/rezepte/')[1]
        recipe_id = recipe_id.split("/")[0]

        # skip if already known
        if int(recipe_id) in known_recipes:
            continue

        recipe_name = soup.find('div', {'class': 'content'})

        if recipe_name:

            recipe_name = str(recipe_name)
            recipe_name = recipe_name.split('<h1 class="page-title">')[1]
            recipe_name = recipe_name.split('</h1>')[0]

            print(str(recipe_name))

        ingredients = soup.find('div', {'id': 'recipe-incredients'})

        if not ingredients:
            # if no ingredients were found, print an error message
            print("no ingredients found for " + link)
            continue

        # all seems ok
        # add recipe to db
        known_recipes.append(int(recipe_id))

        cur.execute("Insert into recipe_names (recipe_id, recipe_name) Values (?, ?)",
                    (int(recipe_id), recipe_name))

        for ingredient in ingredients.findAll("td"):

            # skip amounts
            if str(ingredient).count('class="amount"') > 0:
                continue

            ingredient_name = ingredient.getText().split()[0].split('(')[0].split(',')[0]

            # check if ingredient already existing
            existing = cur.execute("SELECT * FROM ingredient_names WHERE ingredient_name=?",
                                   (ingredient_name,)).fetchone()

            if existing:
                # we already have that ingredient present
                print(ingredient_name + " (already present)")
                ingredient_id = existing[0]
            else:
                # check in unwanted ingredients
                if ingredient_name in unwanted_words:
                    print("-- UNWANTED ingredient: {}".format(ingredient_name))
                    continue
                # insert ingredient into db
                print(ingredient_name)
                cur.execute("INSERT INTO ingredient_names (ingredient_name) VALUES (?)", (ingredient_name,))
                ingredient_id = cur.lastrowid

            cur.execute("INSERT INTO recipe_ingredients (recipe_id, ingredient_id) VALUES (?, ?)",
                        (int(recipe_id), int(ingredient_id)))

        con.commit()

        number_of_recipes += 1

