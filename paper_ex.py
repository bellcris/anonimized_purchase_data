import difflib

pu = {1: ['o', 'e', 's', 'b'],
      2: ['b', 's', 'e', 'f', 'w'],
      3: ['m', 's'],
      4: ['wi', 's', 'm'],
      5: ['m', 'o', 'b', 's', 'f', 'e'],
      }

similarities = {}
max_ratio_per_transaction = []
p_nr = 1
k_value = 2

for p in pu:
    similarities[p] = []
    max_ratio = 0.0
    max_ratio_to = 0
    p_nr = p

    if p > 1:
        similarities[p - 1].sort(key=lambda x: x[1], reverse=True)
    for p_i in pu:
        if p_i is p:
            continue
        sm = difflib.SequenceMatcher(None, pu[p], pu[p_i])
        similarities[p].append((p_i, sm.ratio()))

        if sm.ratio() > max_ratio:
            max_ratio = sm.ratio()
            max_ratio_to = p_i

        if max_ratio_to > 0:
            max_ratio_per_transaction.append((max_ratio, max_ratio_to))

similarities[p_nr].sort(key=lambda x: x[1], reverse=True)

max_ratio_per_transaction.sort(reverse=True)

print('{}\n\n'.format(similarities))

print('{}\n\n'.format(max_ratio_per_transaction))

merged = []
group_elem = 0
merging_lists = []
k_pu = {}

for similar in max_ratio_per_transaction:
    line = ''
    if similar[1] not in merged:
        try:
            if merging_list_tid:
                merging_lists.append(merging_list_tid)
        except (UnboundLocalError, NameError):
            pass
        merging_list_tid = []
        merged.append(similar[1])
        line += '{}: {} + '.format(len(k_pu), similar[1])
        merging_list_tid.append(similar[1])
        for transaction in similarities[similar[1]]:
            if len(merged) == len(pu) and group_elem == 0:
                group_elem += 1
                line += 'goes to UP^'
                print(line)
                break
            # only add transactions which have a sm.ratio over 0.0 OR have 0.0 with all
            if transaction[0] not in merged:
                if transaction[1] == 0.0:
                    print('adding a null for {}'.format(transaction[0]))
                line += '{}, '.format(transaction[0])
                merging_list_tid.append(transaction[0])
                merged.append(transaction[0])
                group_elem += 1
            if group_elem == k_value - 1:
                group_elem = 0
                break
else:
    try:
        merging_lists.append(merging_list_tid)
    except UnboundLocalError:
        pass

print('{}\n\n'.format(merging_lists))

