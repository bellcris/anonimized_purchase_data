Analyzing anonymized buying habits
--------------------------------------------------------------

This is the code for the BA-Thesis "Analyzing anonymized buying habits.". 
The main goal is to investigate if analysis of buying habits is possible using data that completely maintains the anonymity of the buyers.
The code contians tool used to generate purchases based on recipes from chefkoch.de and then mine association rules in this purchases and all other scripts used to generate data for the paper.


Installation
-------------
To use the command-line interface just make sure all sql files are in the configured directories and type `python3 assoc_rules.py`.
Using the web interface: 
To use the web component you need to install [flask](http://flask.pocoo.org/) - a micro web framework for Python.

- It is recommended to run everything in a **virtual environment** [venv](https://docs.python.org/3/tutorial/venv.html) where you can install **flask** and run the whole project - **OPT A**. Alternatively flask can be installed globally and everything can be run from ./web_component, **OPT B**.

Steps:
- make sure Python 3 is installed and working. The entire project is designed targeting Python 3 and tested on 3.5 and 3.6.
- [pip](https://pypi.python.org/pypi/pip) (package manager for Python) is also required.
To install on Ubuntu/Debian: `sudo apt-get install python3-pip`, on CentOS/Red Hat: `yum install python-pip`<br/>
- please make sure pip is up to date: on Ubuntu/Debian: `sudo -H pip3 install --upgrade pip`, CentOS/Red Hat: `pip install -U pip`<br/>

**OPT A**, using a virtual environment:
- install the virtual environment (**venv**) module: on Ubuntu/Debian run: `apt-get install python3-venv` for CentOS/Red Hat `pip install -U virtualenv`
- `python3 -m venv /path/to/your/virtual_env/venv_name`
- change to the virtual environment `cd /path/to/your/virtual_env/venv_name`
- copy the ./web_component files over: `cp -R /path/to/git/repo/web_component/* ./`
- copy the core files to the venv: `cp -R /path/to/git/repo/core ./`
- activate your virtual environment `source /path/to/your/virtual_env/venv_name/bin/activate`<br/>
The command prompt should now have a `(venv)` in front of the command line.<br/>

Both **OPT A** and **OPT B**:
- install Flask in the venv: `pip3 install Flask` **OPT A** OR `sudo -H pip3 install Flask` to install globally **OPT B**.<br/>
To verify that Flask is correctly installed run `pip3 list` and see it listed among all available modules.
- the path to the sql data files (chefkoch.w.data.500.db or chefkoch.w.data.200.db) and to the results output folder need to be set via `sql_data_dir` and `results_dir` variables in `./core/assoc_rule_finder.py`.
- run the app with `python3 app.py`
- for **OPT A** type `deactivate` at any time to exit the virtual environment;

![screenshot](./paper/images/web_screenshot.png?raw=true "anonimized_purchase_data")