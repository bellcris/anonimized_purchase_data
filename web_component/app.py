from web_app import app as application
if __name__ == "__main__":
    application.run(port=8080, host='0.0.0.0', debug=True)

'''
to start just run: 
python app.py OR python3 app.py

'''