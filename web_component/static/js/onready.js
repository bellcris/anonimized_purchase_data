var filename = "";
var timer = 0;
var crtUri = "";
var support = $('[name="support"]');
var confidence = $('[name="confidence"]');
var transactions = $('[name="transactions"]');
var k_value = $('[name="k_value"]');
var single_run = $('[name="single_run"]');
var limit_transactions_output = false;
var k_step = 2;
var k_values = [];
var s, sFP;
var assoc_rules_sets = [];
var assoc_rules_one = [];
var debug = true; //if enabled node numbers (IDs) are shown.

$(document).ready(function () {
    var section_titles = $('.container .section_title');
    //hide all elements
    section_titles.each(function(){
        showHideSectionTitles($(this));
    });
    section_titles.on("click", function(){
        showHideSectionTitles($(this));
    });
    showHideKDivs(k_value.val());

    //adding triggers to elements
    $('.setup .settingsBtn').on("click", function(){
        ($(this).next()).toggle();
    });
    $('.setup .close').on("click", function(){
        ($(this).parent()).hide();
    });

    $('.expand_btn').on("click", function(){
        if ($(this).next().next().is(":visible") === true)
            ($(this).next().next()).toggleClass('area_expanded');
    });

    transactions.on("change keyup", function(){
        calc_tot_articles($(this).val());
    });
    calc_tot_articles(transactions.val());

    k_value.on("change keyup", function(){
        showHideKDivs($(this).val());
    });
    $('.plus').on("click", function(){
        $('.fp_tree, .cond_fp_tree, .kdiv').toggle();
        $('.assoc_rules .content .table').toggleClass('mR35');
        $(document.body).addClass('sshotBody');
        $('.results').addClass('sshotBox');
        $('.kdiv .results').addClass('sshotFont');
    });

});