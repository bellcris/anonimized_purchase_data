if (typeof sigma !== 'undefined') {
    sigma.classes.graph.addMethod('neighbors', function(nodeId) {
        var k,
        neighbors = {},
        index = this.allNeighborsIndex[nodeId] || {};
        for (k in index)
            neighbors[k] = this.nodesIndex[k];
        return neighbors;
    });

    var i,
	s,
	N = 5,
	E = 20,
	g = {
	  nodes: [],
	  edges: []
	},
	nColor = '#245DB3',
	eColor = '#a0a0a9',/* light a6bddb OR dark 4773AC*/
	fadeColor = '#ece7f2';
	aColor = [];
	aColor['FP'] = '#476183';

    if (typeof demoGraph !== 'undefined' && demoGraph === true) {
        // Generate a random graph:
        for (i = 0; i < E; i++)
          g.edges.push({
            id: 'e' + i,
            source: 'n' + (Math.random() * N | 0),
            target: 'n' + (Math.random() * N | 0),
            size: Math.random(),
            color: eColor,
            /*type: 'curve',*/
          });

        //console.log(g.edges);
        for (i = 0; i < N; i++){
            nSize = getNodeSize('n' + i);
            if (nSize < 1)
                nSize = 0.5;
            nSize = nSize/10;
            console.log('n' + i + ' has ' + nSize + ' (' + nSize*10 + ')');
            g.nodes.push({
                id: 'n' + i,
                label: 'Node ' + i,
                x: Math.random(),
                y: Math.random(),
                size: nSize,
                color: nColor
            });
        }
        startSigma(g);
    }
}

function getNodeSize(nodeId) {
    nrEdges = 0;
    for (edge in g.edges) {
        if (g.edges[edge].source !== 0 && (g.edges[edge].source === nodeId || (g.edges[edge].source !== nodeId && g.edges[edge].target === nodeId)))
            nrEdges++;
    }
    return nrEdges;
}

function addEdge(from, to, sameSize, graphData) {
    if (arguments.length > 2 && sameSize === true) {
        var nSize = 0.5;
    } else {
        var nSize = Math.random();
    }
    var edge = {
        id: 'e' + from + '_' + to,
        source: from,
        target: to,
        size: nSize,
        color: eColor,
        /*type: 'curve',*/
    };

    //console.log(edge);

    if (arguments.length > 3) {
        graphData.edges.push(edge);
    } else {
        g.edges.push(edge);
    }

}

function addNodeById(id, sameSize, graphData) {
    if (arguments.length > 2 && typeof graphData !== 'undefined') {
        exists = checkIfNodeExists(graphData, id);
    } else {
        exists = checkIfNodeExists(g, id);
    }
    if (exists)
        return false;

    if (arguments.length > 1 && (typeof sameSize === 'boolean' || typeof sameSize === 'number')) {
        if (typeof sameSize === 'boolean')
            nSize = 0.5;
        else if (typeof sameSize === 'number')
            nSize = sameSize;
    } else {
        nSize = getNodeSize(id);
        console.log(nSize);
        if (nSize < 1)
            nSize = 0.5;
        nSize = nSize/10;
    }
    label = noIdsLabel(id);

    var node = {
        id: id,
        label: label,
        x: Math.random(),
        y: Math.random(),
        size: nSize,
        color: nColor
    };
    if (arguments.length > 2) {
        graphData.nodes.push(node);
    } else {
        g.nodes.push(node);
    }
}

function addNodeByLabel(label, sameSize, graphData, id, x, y, color) {

    if (arguments.length > 1 && (typeof sameSize === 'boolean' || typeof sameSize === 'number')) {
        if (typeof sameSize === 'boolean')
            nSize = 0.5;
        else if (typeof sameSize === 'number')
            nSize = sameSize;
    } else {
        nSize = getNodeSize(id);
        if (nSize < 1)
            nSize = 0.5;
        nSize = nSize/10;
    }
    if (typeof color === 'string' && color in aColor) {
        color = aColor[color];
    } else
        color = nColor;

    if (typeof x !== 'number')
        x = Math.random();
    if (typeof y !== 'number')
        y = Math.random();

    if (arguments.length < 4)
        if (arguments.length > 2 && typeof graphData === 'object') {
            id = graphData.nodes.length;
        } else {
            id = g.nodes.length;
        }

    if (arguments.length > 2 && typeof graphData !== 'undefined') {
        exists = checkIfNodeExists(graphData, id);
    } else {
        exists = checkIfNodeExists(g, id);
    }
    if (exists)
        return false;

    if (debug)
        label = '(' + id + ') ' + label;
    var node = {
        id: id,
        label: label,
        x: x,
        y: y,
        size: nSize,
        color: color
    };

    if (arguments.length > 2) {
        graphData.nodes.push(node);
    } else {
        g.nodes.push(node);
    }
}

function startSigma(g, noStarLayout) {
    // Instantiate sigma:
    s = new sigma({
        graph: g,
        /*container: 'graph_container', */
        renderer: {
            container: document.getElementById('graph_container'),
            type: 'canvas'
        },
        settings: {
          labelThreshold:1,
          labelColor: 'node',
          //for screenshots
          /*
          defaultLabelSize:22,
          labelColor: '#000000',
          */
          sideMargin: 2
        }
    });

    if (typeof s !== 'undefined') {
        $('.assoc_rules .content').addClass('mHGraph');
        $('.assoc_rules .content .graph').show();
    }

    s.bind('clickNode', function(e) {
        var nodeId = e.data.node.id,
        toKeep = s.graph.neighbors(nodeId);
        toKeep[nodeId] = e.data.node;
        var allNodes = Object.keys(toKeep).map(function (key) { return toKeep[key]; });;

        s.graph.nodes().forEach(function(n) {
            if (toKeep[n.id])
                n.color = nColor/*n.originalColor*/;
            else
                n.color = fadeColor;
        });

        s.graph.edges().forEach(function(e) {
          if (toKeep[e.source] && toKeep[e.target])
            e.color = e.originalColor;
          else
            e.color = fadeColor;
        });

        // call the refresh method to update colors.
        s.refresh();
    });

    s.bind('clickStage', function(e) {
        s.graph.nodes().forEach(function(n) {
          n.color = nColor;
        });

        s.graph.edges().forEach(function(e) {
          e.color = eColor;
        });

        // Same as in the previous event:
        s.refresh();
    });
    if (arguments.length == 1) {
        //@todo: this should to be in separate a function
        window.setTimeout(betterLayout, 1500);
        function betterLayout() {
            s.startForceAtlas2();
            window.setTimeout(stopJitter, 1000);
        }
        function stopJitter(){
            s.stopForceAtlas2();
        }
    }

    return s;
}

function startSigmaFPTree(g, targetId) {
    // Instantiate sigma:
    s = new sigma({
        graph: g,
        /*container: 'graph_container', */
        renderer: {
            container: document.getElementById(targetId),
            type: 'canvas'
        },
        settings: {
          labelThreshold:1,
          labelColor: 'node',
          sideMargin: 4
        }
    });
    $('.fp_tree .content').addClass('mHGraph');
    $('.fp_tree .content .graph').show();
    return s;
}

function refreshGraph(sigmaObj) {
    sigmaObj.refresh();
}

function checkIfNodeExists(graphData, id) {
    if (typeof graphData.nodes.find(o => o.id === id) !== 'undefined')
        return true;
    return false;
}
