function showHideSectionTitles(obj) {
    (obj.next()).toggle();
    obj.toggleClass('section_expanded');
    (obj.children("img")).toggleClass("imgUpside");
    if ((obj.prev()).hasClass('expand_btn')) {
        if (obj.next().is(":visible") === true)
            (obj.prev()).removeClass('cuNA');
        else
            (obj.prev()).addClass('cuNA');
    }
}

function showHideKDivs(k) {
    if (k === '1') {
        $('.kdiv').hide();
    } else {
        $('.kdiv').show();
    }
}

function showSection(sectionName) {
    if ($(sectionName+' .content').is(":visible") === false)
        showHideSectionTitles($(sectionName+' .section_title'));
}

function hideSection(sectionName) {
    if ($(sectionName+' .content').is(":visible") === true)
        showHideSectionTitles($(sectionName+' .section_title'));
}

function start(){
    error = true;
    if (start_data != null && start_data.length > 5) {
        start_data = JSON.parse(start_data);
        error = start_data.error;
        if (start_data.purchases_list !== 'undefined' && typeof start_data.purchases_list === 'object') {
            draw_all_transactions();
        }
        //console.log(start_data.purchases_list)
    }
    if (error) {
        $('.u_message').addClass('danger').show();
    }
}

function draw_all_transactions() {
    $('<div><span class="b">All transactions </span> </div>').appendTo('.container .transactions .table_all');
    for (let i in start_data.purchases_list) {
        //start_data.purchases_list[i]
        if (i === 10 && limit_transactions_output)
            break;
        $('<div><span class="b">' + i + '.</span> ' + start_data.purchases_list[i].toString().replace(/,/g , ', ') + '</div>').appendTo('.container .transactions .table_all');
    }
}

function calc_tot_articles(nr_transactions) {
    $('.tot_articles').html(Math.floor(nr_transactions * avg_art_transaction));
}

function run(supportIn, confidenceIn) {
	var supportV;
	var confidenceV;
	(arguments.length > 0 && supportIn !== "") ? supportV = supportIn : supportV = support.val();
	(arguments.length > 1 && confidence !== "") ? confidenceV = confidenceIn : confidenceV = confidence.val();
	if (support !== '' && confidence !== '') {
	    assoc_rules_sets = [];assoc_rules_one = [];
	    hideSection('.container .assoc_rules');
	    hideSection('.container .fp_tree');
	    var k = parseInt(k_value.val());
	    if (k > 1) {
            crt_k_step = 0;
	    	var c = 2;var i = 0;
	        calculateStepForK(k, single_run.prop('checked'));
	        console.table(k_values);
	        //empty target divs
	        var $targetDiv = $('.container .correlations .content');
	        $targetDiv.html('');
	        var $targetDivTr = $('.container .transactions_min_supp .content');
	        $targetDivTr.html('');
	        for (i in k_values) {
                var newDivAR = $('<div class="results corr_g' + k_values[i] + '" id="' + k_values[i] + '">' + '' + '</div>');
                var newDivTr = $('<div class="results corr_g' + k_values[i] + '" >' + '' + '</div>');
                newDivTr.attr('id',  'tr_' + k_values[i]);
                var content = '<div class="highlightTxt biggerTxt">k = ' + k_values[i] + '';
                content += '<input type="button" class="fR cgraph" title="' + k_values[i] + '" value="show graph" name="cgraph"/>';
                content += '<input type="button" class="fR cfilter" title="' + k_values[i] + '" value="apply filter" name="cfilter"/>';
                if (k_values[i] === 1) {
                    content += '<span class="fR">confidence:<input id="min" name="min" type="number" class="" placeholder="min" min="1" max="100" size="2">';
                    content += '<input id="max" name="max" type="number" placeholder="max" min="1" max="100" size="2"></span>';
                }
                content += '</div><table id="tbl_k' + k_values[i] + '" class="display dataTbl compact" width="100%"></table><div class="L">L</div>';
                newDivAR.html(content);
                newDivAR.appendTo($targetDiv);
                newDivTr.appendTo($targetDivTr);
                $('.transactions_min_supp .t_info').html('');
	        }

            /*$('.dataTbl').DataTable({
                data: [[1,2],[2,2]],
                columns: [{ title: "left" },{ title: "right" }]
            });*/
	        callRun_r(supportV, confidenceV);
	        /*while (c <= k) {
	            console.log('call /run with k = '+c);
	            $('<div class="graph corr_g' + c + '" style="left:calc(1.5em + ' +  i*660+ 'px)"> ' + c + '</div>').appendTo('.container .correlations .content');
                c += k_step;
                i++;
	        }*/
            return true;
	    }

		$.ajax({
			url: '/run',
			data: {support:supportV, confidence:confidenceV},
			method: 'POST'
        }).done(function (response) {
            if (typeof response === 'string' && response.length > 5){
                rsp = JSON.parse(response);
                // display transactions w/ min_supp
                draw_transactions('.container .transactions .table_min_supp', rsp.total_transactions, rsp.purchases_min_supp);

                // display L info:
                L = LtoString(rsp.L);
                var target = $('.container .min_supp_info .content');
                target.html($('<div><span class="b">L = </span> ' + L + '<br/><br/>' + rsp.message + '</div>'));
                showSection('.container .min_supp_info');

                // display the PF tree
                gFPTree = {nodes: [], edges: []};
                for (i in rsp.FPTree) {
                    node = rsp.FPTree[i];
                    var label = '';
                    if (typeof node['val'] === 'number' && node['val'] > 0) {
                        label = idToNamesShort(node['val']) + ':' + node['supp'];
                    } else {
                        label = node['val'];
                    }
                    addNodeByLabel(label, 0.3, gFPTree, node['id'], '', '', 'FP');
                    if (typeof node['p'] === 'number' && node['p'] > 0)
                        addEdge(node['p'], node['id'], true, gFPTree);
                }
                $('#fptree_container').html('');
                showSection('.container .fp_tree');
                sFP = startSigmaFPTree(gFPTree, 'fptree_container');

                var listener = sigma.layouts.dagre.configure(sFP, {rankdir: 'TB'});
                sigma.layouts.dagre.start(sFP);

                sFP.refresh();
                //window.setTimeout(refreshGraph(sFP), 500);
                // placeholder to display the cond PF tree

                // display the association rules and update the association rules graph
                tableContainer = $('.container .assoc_rules .table');
                tableContainer.html('');
                outputAssocRules(tableContainer, rsp.assoc_rules_hr, '', 'graph_container');
			}
		});
	}
}

function my_reduce(item, nodes) {
    if ((typeof item === 'object' || typeof item === 'array') && item.length > 0) {
        return item.reduce(idToNames, '').slice(0, -2);
    } else if (typeof item === 'number') {
        return start_data.articles[item];
    }
}

function my_reduce_for_id(item) {
    if ((typeof item === 'object' || typeof item === 'array') && item.length > 0) {
        return item.join('_');
    } else if (typeof item === 'number') {
        return item;
    }
}

function idToNames(list, item) {
    return list + start_data.articles[item] + ', ';
}

function noIdsLabel(label) {
    return label.toString().replace(/\s\[[0-9]+\]/gi , '');
}

function idToNamesShort(id) {
    let name = start_data.articles[id].replace(/\[\d+\]/g , '');
    if (name.length > 7)
        return name.substr(0, 5);
    else 
        return name;
}

function settings() {
    let t_val = parseInt(transactions.val());
	if (typeof t_val === 'number' && t_val > 0) {
		$.ajax({
			url: '/settings',
			data: {transactions_nr:t_val},
			method: 'POST'
        }).done(function (response) {
            if (typeof response === 'string' && response.length >5) {
                console.log(response);
            }
        });
    }
}

function calculateStepForK(k, single_run) {
    k_values = [1];
    if (arguments.length > 1 && single_run === true) {
        k_values.push(k);
        return true;
    }
    k_step = Math.ceil((k - 2)/10);
    if (k_step < 2)
        k_step = 2;
    let c = 2;
    while (c <= k) {
        k_values.push(c);
        c += k_step;
    }
}

var crt_k_step = 0;
function callRun_r(supp, conf) {
    if (k_values.length > 0) {
        var k_val = k_values.shift();
        $.ajax({
            url: '/run',
            data: {support:supp, confidence:conf, k_value:k_val},
            method: 'POST'
        }).done(function (rsp) {
            if (typeof rsp === 'string' && rsp.length > 5){
                rsp = JSON.parse(rsp);
                //display L, min support, transaction nr and assoc. rules info
                var targetDivPath = '.container .correlations .content .corr_g'+k_val;
                var $targetDiv = $(targetDivPath);
                var t_min_supp_txt = '';
                if (typeof rsp.message !== 'undefined') {
                    txt_pref = '<span class="highlightTxt">' + rsp.message + '</span><br/><br/>';
                }
                if (typeof rsp.purchases_min_supp !== 'undefined') {
                    t_min_supp_txt = '</b>, from which <b>' + Object.keys(rsp.purchases_min_supp).length + '</b> with <b>min support items';
                }
                //min supp.: ' + rsp.min_support + ',
                txtL = txt_pref + '<b>min supp. factor: ' + rsp.min_support_factor;
                txtL += ', transactions: ' + rsp.total_transactions + t_min_supp_txt + '<br/><br/>L set</b>' + LtoString(rsp.L) + '';
                if (typeof rsp.in_base !== 'undefined') {
                    txtL += '<br/><br/><b>' + Object.keys(rsp.in_base).length + ' rules from baseline:&nbsp;</b><br/>';
                    for (rule in rsp.in_base) {
                        txtL += rsp.in_base[rule][0].replace(" ", "&nbsp;") + '&nbsp;=>&nbsp;' + rsp.in_base[rule][1].replace(" ", "&nbsp;") + '; ';
                    }
                }
                if (typeof rsp.in_base_inv !== 'undefined' && false) {
                    txtL += '<br/><br/><b>' + Object.keys(rsp.in_base_inv).length + ' rules from baseline (INV):&nbsp;</b>';
                    for (rule in rsp.in_base_inv) {
                        txtL += rsp.in_base_inv[rule][0] + '>' + rsp.in_base_inv[rule][1] + '; ';
                    }
                }
                $(targetDivPath + ' .L').html(txtL);

                if (typeof rsp.assoc_rules_hr === 'object') {
                    outputAssocRulesTbl(rsp.assoc_rules_hr, k_val);
                }
                showSection('.container .correlations');

                //display transactions with min support
                if (typeof rsp.purchases_min_supp === 'object') {
                    targetDivPath = '.container .transactions_min_supp .content .corr_g'+k_val;
                    draw_transactions(targetDivPath, rsp.total_transactions, rsp.purchases_min_supp, '<span class="highlightTxt biggerTxt">k = ' + k_val + '</span>');
                    $('.transactions_min_supp .t_info').html('new data!');
                }


                /*
            {% for x in range(0, 8) %}
            <div class="graph" style="left:calc(1.5em + {{660*x}}px)">{{ x }}</div>
            {% endfor %}

            style="left:calc(1.5em + ' +  i*660+ 'px)"
                */
            }
            crt_k_step++;
            callRun_r(supp, conf);
        });
    } else
        return false;
}

function LtoString(objL) {
    //L = JSON.stringify(response.L).replace(/,/g , ', ');
    let itemsL = [];
    let L = '';
    if (objL.length === 0)
        return ' L was empty';
    let index;
    for (index in objL) { //why does .map() not work?
        itemsL.push({name: objL[index][0], support: objL[index][1]});
    }
    itemsL.sort(function (a, b) {
        return b.support - a.support;
    });
    for (let item in itemsL) {
        if (typeof start_data.articles[itemsL[item].name] !== 'undefined')
            L += start_data.articles[itemsL[item].name].replace(" ", "&nbsp;") + ':&nbsp;' + itemsL[item].support + ', ';
        else
            L += 'UNKNOWN?!' + itemsL[item].name + ':&nbsp;' + itemsL[item].support + ', ';
    }

    let nr_items = parseInt(index)+1;
    return ' (' + nr_items + ' items): ' + L.substr(0, L.length-2);
}

function outputAssocRules(tableContainer, assoc_rules, title, graphContainerId) {
    g = {nodes: [], edges: []};
    outputGraph = false;
    if (arguments.length > 3 && typeof graphContainerId === 'string') {
        g = {nodes: [], edges: []};
        outputGraph = true;
    }
    // feed rules to DataTables
    outputAssocRulesTbl(assoc_rules, 0);

    nodes = [];
    edges = []
    nodesSize = {};
    // add rules to graph
    for (i in assoc_rules) {
        var bgColorClass = '';

        left = assoc_rules[i][0];
        right = assoc_rules[i][1];

        //add to graph
        if (arguments.length > 2 && typeof outputGraph === 'boolean' && outputGraph === true) {
            if (left in nodesSize)
             nodesSize[left] = nodesSize[left] + 1;

            if (right in nodesSize)
                nodesSize[right] = nodesSize[right] + 1;

            if (nodes.indexOf(left) < 0) {
                nodes.push(left);
                nodesSize[left] = 1;
            }
            if (nodes.indexOf(right) < 0) {
                nodes.push(right);
                nodesSize[right] = 1;
            }
            edges.push(left + '_' + right);
        }
    }

    for (node in nodes) {
        addNodeById(nodes[node], nodesSize[nodes[node]]);
    }
    for (edge in edges) {
        var parts = edges[edge].split('_');
        addEdge(parts[0], parts[1]);
    }


    if (outputGraph) {
        if (window.graph) {
            window.graph.kill();
        }
        $('#'+graphContainerId).html('');
        window.graph = null;
        showSection('.container .assoc_rules');
        s = null;
        s = startSigma(g);
        s.refresh();
    }
}

function outputAssocRulesTbl(assoc_rules, k_val) {
    var id = '#tbl_k'+k_val;
    var idMin = '#min'; 
    var idMax = '#max';
    if ($.fn.DataTable.isDataTable(id)) {
        $(id).DataTable().clear().destroy();
    }

    if (typeof $(idMin).val() === 'string') {
        $.fn.dataTable.ext.search.push(
            function(settings, data, dataIndex) {
                var min = parseInt( $(idMin).val());
                if (min > 10) 
                    min = min / 100;
                else 
                    min = min / 10;
                var max = parseInt( $(idMax).val()) / 10;
                if (max > 10) 
                    max = max / 100;
                else 
                    max = max / 10;
                var conf = parseFloat(data[2]) || 0; // use data for the conf column
         
                if ( (isNaN(min) && isNaN(max)) || (isNaN(min) && conf <= max) || 
                     (min <= conf && isNaN(max)) || (min <= conf && conf <= max) )
                {
                    return true;
                }
                return false;
            }
        );
    }

    $(id).DataTable( {
        data: assoc_rules,
        columns: [
            { title: "r. antecedent" },
            { title: "r. consequent" },
            { title: "conf." },
            { title: "lift" },
            { title: "conv." },
            { title: "inv." },
            /*{ title: "base" }*/
        ], 
        "lengthMenu": [[10, 50, 100, 300, 1000, -1], [10, 50, 100, 300, 1000, "All"]]
    });

    $('.cfilter').on("click", function(){
        $('#tbl_k' + $(this).attr('title')).DataTable().draw();
    });
}

function draw_transactions(targetDiv, transactions, transactions_min_supp, titleTxt) {
    if (typeof transactions_min_supp !== 'object')
        return false;
    var tText = '';
    if (arguments.length > 3 && typeof titleTxt !== 'undefined')
        tText = titleTxt;
    $(targetDiv).html('<div> ' + tText + '<span class="b">Transactions after min. support was applied</span></div>');
    for (i=1; i < transactions+1; i++) {
        if (i === 10 && limit_transactions_output)
            break;
        line = '';
        bgColorClass = '';
        try {
            if (typeof transactions_min_supp !== 'undefined' && typeof transactions_min_supp[i] === 'object') {
                line = ' <b>[tot: ' + Object.keys(transactions_min_supp[i]).length + ']</b>: ';
                line += transactions_min_supp[i].toString().replace(/,/g , ', ');
            } else {
                bgColorClass = 'bgMissing';
            }
            $('<div class="' + bgColorClass + '"><span class="b">' + i + '.</span> ' + line + '</div>').appendTo(targetDiv);
        }
        catch(err) {
            console.log(err);
        }
    }
}

