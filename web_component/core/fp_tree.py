from core.util import Util
from core.util import BColors
from core.node import Node


class FPTree:
    def __init__(self):
        self.__root = None
        self.nr_of_nodes = 0
        self.current_level = 0
        self.height = 0
        self.hdr_table = {}
        self.tmp_nodes = []
        self.util = Util(False)
        self.colors = BColors()

    '''
    inserts a transaction into the tree
    IF there is no node, a root is created and the transaction added as a branch. 
    ELSE as long as the same paths exists the counters [support] are incremented
    '''
    def insert(self, transaction, counter=1):
        self.util.print(self.colors.colorize('inserting: {}'.format(transaction), self.colors.GR))
        if not self.__root:
            self.__root = self.new_node('root')
            self.add_branch(transaction, self.__root, 1, counter)
        else:
            self.current_level = 0
            parent_node = self.__root
            rest_transaction = list(transaction)
            for item in transaction:
                self.current_level += 1
                found = False
                self.util.print('look for {} at {}'.format(item, self.current_level))
                for child in parent_node.get_children():
                    if child.value == item:
                        self.util.print(self.colors.colorize('found {}'.format(child) +
                                                             'lvl {} and val {}'.format(self.current_level, item)))
                        child.support += counter
                        rest_transaction.remove(item)
                        parent_node = child
                        found = True
                        break
                if not found:
                    break
            self.util.print('rest transaction: {}'.format(rest_transaction))
            self.add_branch(rest_transaction, parent_node, self.current_level, counter)

    '''
    adds a branch (from a transaction) to the FP tree
    '''
    def add_branch(self, transaction, node, level=1, counter=1):
        if len(transaction) > 0 and node is not None:
            self.util.print('add_branch() to {}'.format(node))
            i = 0
            prev_node = node
            while i < len(transaction):
                a_node = self.new_node(transaction[i], level, counter)
                if prev_node is not None:
                    prev_node.add_child(a_node)
                    a_node.add_parent(prev_node.nid)
                prev_node = a_node
                i += 1
                level += 1
            return prev_node
        return None

    '''
    updated the header table after each node insert
    '''
    def update_hdr_table(self, value, new_node):
        if value not in self.hdr_table:
            self.hdr_table[value] = new_node.nid
        else:
            next_node_id = self.hdr_table[value]
            next_node = self.get_node_by_nid(next_node_id)
            while next_node_id is not 0:
                next_node = self.get_node_by_nid(next_node_id)
                next_node_id = next_node.next_node_id
            next_node.next_node_id = new_node.nid

    '''
    :return node
    return a node by nid
    '''
    def get_node_by_nid(self, nid):
        for node in self.tmp_nodes:
            if node.nid is nid:
                # print(node)
                return node
        return None

    '''
    :return node
    returns the root node
    '''
    def get_root(self):
        return self.__root

    '''
    :return []
    returns a list of all nodes for a given level
    '''
    def get_nodes(self, level):
        return [node for node in self.tmp_nodes
                if node.level is level]

    '''
    :return int
    returns the count of all nodes for a given level
    '''
    def get_nodes_count(self, level):
        count = 0
        for node in self.tmp_nodes:
            if node.level == level:
                count += 1
        return count

    '''
    :return node
    creates a new node
    '''
    def new_node(self, value=None, level=0, counter=1, nid=-1):
        try:
            if nid is -1:
                nid = self.nr_of_nodes + 1
            node = Node(nid, value, counter, level)
            self.nr_of_nodes += 1
            self.tmp_nodes.append(node)
            self.update_hdr_table(value, node)
            if level > self.height:
                self.height = level
            return node
        except Exception:
            print('err: new_node()')
            return None

    '''
    returns the path from a given node up to the root
    '''
    def get_path_up(self, node, info=False):
        path = []
        while node is not self.__root:
            node = self.get_node_by_nid(node.parent)
            if node is self.__root:
                break
            if info is 'val':
                path.append(node.value)
            elif info is 'nid':
                path.append(node.nid)
            else:
                path.append(node)
        return path

    '''
    draws the tree to the CLI
    '''
    def draw(self, minimal=False):
        from operator import attrgetter
        self.tmp_nodes.sort(key=attrgetter('level', 'parent'))

        level = 0
        max_width = 0
        max_width_lvl = 0
        if minimal:
            spacer = ' ' * 12
            spacer2 = ' ' * 22
            spacer3 = ' ' * 8
            spacer4 = ' ' * 4
        else:
            spacer = ' ' * 18
            spacer2 = ' ' * 36
            spacer3 = ' ' * 15
            spacer4 = ' ' * 0
        while level <= self.height:
            width = self.get_nodes_count(level)
            if width > max_width:
                max_width = width
                max_width_lvl = level
            level += 1
        level = 0

        while level <= self.height:
            nodes = self.get_nodes(level)
            level_spacer = int((max_width-len(nodes))/2)
            row1 = spacer2*level_spacer
            row2 = spacer2*level_spacer

            for node in nodes:
                node.print_minimal = minimal
                row1 += ' {} {}'.format(node, spacer4)
                children = node.get_children()
                children_nr = len(children)
                if children_nr > 2:
                    # row1 = spacer2 + row1
                    row2 += spacer3 + ' /|\ '
                elif len(children) > 1:
                    # row1 = spacer + row1
                    row2 += spacer3 + ' / \ '
                elif len(children) > 0:
                    row2 += spacer3 + '  |  '
                row2 += spacer
                node.print_minimal = False
            print(row1)
            print(row2)
            level += 1
        print(self.colors.colorize('hdr_tbl:{}'.format(self.hdr_table)))

    '''
    prints tree in order
    '''
    def print_inorder(self):
        self.__print_inorder_r(self.__root)

    '''
    prints the current node and recursively calls itself for all current node children 
    '''
    def __print_inorder_r(self, current_node):
        if not current_node:
            return
        for child in current_node.get_children():
            self.__print_inorder_r(child)
        print(current_node)

    '''
    prints just the root node
    '''
    def print_root(self):
        print(self.__root)

    def return_all_nodes(self, per_level=False):
        all_nodes = []
        if per_level:
            level = 0
            while level <= self.height:
                level_nodes = []
                nodes = self.get_nodes(level)
                for node in nodes:
                    level_nodes.append(node.to_dict())
                level += 1
                all_nodes.append(level_nodes)
        else:
            for node in self.tmp_nodes:
                all_nodes.append(node.to_dict())
        return all_nodes

    '''
    '''
    def __repr__(self):
        output = 'leaf: {}, height: {}\n'.format(len(self.tmp_nodes), self.height)
        if self.__root is not None:
            for node in self.tmp_nodes:
                output += '{}\n'.format(node)
            output += 'hdr_tbl: {}'.format(self.hdr_table)
        return output
