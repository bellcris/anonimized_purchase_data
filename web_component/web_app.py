from flask import Flask, render_template, request, session, g
'''import re
import urllib.request
import os'''
import json
from core import assoc_rules_finder

app = Flask(__name__)
# settings
app.secret_key = '1234'
transactions_nr = 40
min_supp_val = 40
k_anon_naive = True
demo_graph = False
max_L_size = 7
articles = {}


@app.before_request
def init_session():
    global transactions_nr, min_supp_val
    if 'transactions_nr' not in session or session['transactions_nr'] == '':
        session['transactions_nr'] = transactions_nr
    else:
        transactions_nr = session['transactions_nr']
    if 'min_supp_val' not in session or session['min_supp_val'] == '':
        session['min_supp_val'] = min_supp_val
    else:
        min_supp_val = session['min_supp_val']


# initializes the data set and displays the homepage
@app.route('/')
def homepage():
    global articles
    clean = request.args.get('clean')
    if clean:
        session.clear()
        session['transactions_nr'] = ''
    init_session()
    start_data = assoc_rules_finder.start(transactions_nr=transactions_nr)
    print('transactions_nr set to: {}'.format(transactions_nr))
    if start_data['error']:
        return render_template('error.html', message=start_data['message'])

    articles = start_data['articles']
    data = json.dumps(start_data, separators=(',', ':'), ensure_ascii=False)
    return render_template('home.html', data=data, message=start_data['message'],
                           nr_transactions=len(start_data['purchases_list']), nr_articles=len(start_data['articles']),
                           avg_art_transaction=25.63, tot_transactions_db=start_data['tot_transactions_db'],
                           demoGraph=demo_graph, min_supp_val=min_supp_val)


# run handles all data filtering and ARM.
@app.route('/run', methods=['POST'])
def run():
    try:
        support = int(request.form.get('support'))
        session['min_supp_val'] = support
    except ValueError:
        support = min_supp_val
    k_value = request.form.get('k_value')
    confidence = int(request.form.get('confidence')) / 100
    use_k = False
    set_k = False
    if k_value:
        k_value = int(k_value)

        if k_value > 1:
            anonym_data = assoc_rules_finder.anonymize_items(k_value, naive_method=True)
            # return json.dumps({'err': 1})
            if not anonym_data:
                return json.dumps(
                    {'message': 'run() failed with k={}, no anonym_data'.format(k_value), 'k_value': k_value})
            use_k = True
        elif k_value == 1:
            set_k = True
    filter_data = assoc_rules_finder.filter_items(support, use_k=use_k)

    if filter_data:
        rules_data = assoc_rules_finder.find_assoc_rules(use_k=use_k, confidence=confidence, set_k=set_k)
        if set_k:
            rules_data.pop('freq_patterns', None)
            rules_data.pop('FPTree', None)
        return json.dumps({**filter_data, **rules_data, 'k_value': k_value})
    else:
        return json.dumps({'message': 'run() failed with k={}, no filter_data'.format(k_value), 'k_value': k_value})


# the settings API endpoint
@app.route('/settings', methods=['POST'])
def settings():
    global transactions_nr
    t_nr = request.form.get('transactions_nr')
    changes = False
    if int(t_nr) > 0:
        transactions_nr = t_nr
        session['transactions_nr'] = transactions_nr
        changes = True
    return json.dumps({'saved': changes})


# a test function to see if flask is up and running
@app.route('/test')
def test():
    return render_template('test.html')


# displays the tree generated for the example in the paper
@app.route('/pgraph')
def p_graph():
    return render_template('paper_graph.html', message='',avg_art_transaction=25.63)


@app.route('/cont', methods=['POST'])
def cont():
    k_value = request.form.get('k_value')
    return 'new k_value ' + k_value

'''
data set for simple example in the paper: 
(56, 1), (24, 1), (5, 1), (74, 1), 
(74, 2), (5, 2), (24, 2), (62, 2), (3, 2), 
(16, 3), (5, 3) , 
(242, 4), (5, 4), (16, 4), 
(16, 5), (56, 5), (74, 5), (5, 5), (62, 5), (24, 5)
'''