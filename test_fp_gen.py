from core.fp_tree import FPTree

tree = FPTree()

tree.insert(['b', 'd', 'c', 'a'])
tree.insert(['b', 'd', 'a', 'f'])
tree.insert(['c', 'e', 'f'])
tree.insert(['d', 'a',  'e'])
tree.insert(['b', 'a',  'f', 'e', 'g'])


tree.draw()
