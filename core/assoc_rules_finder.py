import os.path
from os import getpid
from os import getppid
from multiprocessing import Process, Queue, Manager
import sqlite3 as lite
import math
import copy
from difflib import SequenceMatcher
# https://stackoverflow.com/questions/6709693/calculating-the-similarity-of-two-lists
from core.util import Util, BColors

'''
Q: 
loosing transactions - is that an issue to address?
                     - (re)calculate min_support_factor
What do if cond_fp_tree for C:
         0
    B:4 / \A:2
A:2 /

<B,A,C:2>, <A,C:2>, <B,C:4> OR <B:4,A:2,C?>, <A,C:2>, 

keyb input: min_support, confidence

_fetch data
drop items with supp < min_supp
order data on support

build FP tree - item(name, count, node_link)
build left header table

for each item in descending order:
- build cond pattern base using the tree
- build own FP tree from cond pattern base (drop all w/ supp < min_supp)
- find freq patterns from own pattern tree (item /\ ALL FP tree paths)


for each rule in rules:
- use confidence to determine direction;

S(X->U) = S(X U Y)/N_tr
C= S(X U Y)/S(X)

'''
sql_data_dir = '../data/'
results_dir = '../_results/'
fname_method = 'kN'
compress_results = True
min_support_default = 3
min_support = min_support_default
min_support_factor = 1
conf_def = 0.1
total_transactions = 0
articles = {}
purchases_list = {}
k_purchases_list = {}
occurrence_per_item = {}

assoc_rules = []
assoc_rules_hr = []
baseline_rules = []
in_baseline_rules = []
in_baseline_rules_inv = []
test_var = 0
debug = True
debug_lvl = 2  # 'cli': 1, web: 2, 'all' = 'web'+'cli' : 3
print_info = False
message = ''
db_transactions_def = 500
transactions_nr_def = 44
max_L_size = 150
set_k_value = 1
set_k_def = False
multi_proc_mode = True
basic_mode = False
paper_mode = False


class ItemSupport:
    def __init__(self, name=None, supp=None):
        self.all = []
        self.add(name, supp)

    def add(self, name, supp):
        if name and supp:
            self.all.append((name, supp))

    def sort(self, desc=True):
        # if desc is true, sort for L - also first sort on value
        if desc:
            self.all.sort(key=lambda x: x[0], reverse=False)
        self.all.sort(key=lambda x: x[1], reverse=desc)

    def support(self, name):
        for item in self.all:
            if item[0] is name:
                return item[1]
        return 0

    def for_json(self):
        return dict(self.all)

    def clean(self):
        self.all = []

    def __repr__(self):
        return repr("items w/ min_supp (id, supp): {}".format(self.all))


itemSupport = ItemSupport()
util = Util(debug)
colors = BColors()


# iterate over all purchases and create the transactions list with unique articles per transaction
def start(printout=False, tester=None, transactions_nr=None):
    global message, print_info, test_var, transactions_nr_def, set_k_value, set_k, paper_mode
    set_k_value = 1
    set_k = set_k_def
    response = {'error': True, 'message': 'something went wrong'}
    test_var = tester
    print_info = printout
    if not transactions_nr:
        transactions_nr = transactions_nr_def
    if paper_mode:
        sql_file = 'chefkoch.w.data.ex.db'
    else:
        sql_file = 'chefkoch.w.data.' + str(db_transactions_def) + '.db'

    if not os.path.isfile(sql_file):
        sql_file_outside = os.path.join(os.path.abspath(sql_data_dir), sql_file)
        if os.path.isfile(sql_file_outside):
            sql_file = sql_file_outside
        else:
            response['message'] = 'sqllite file `{}` not found.'.format(sql_file)
            if print_info:
                print(response['message'] + '\n')
                exit(0)
            else:
                return response

    con = lite.connect(sql_file)
    con.text_factory = str
    with con:
        cur = con.cursor()
        sql = "SELECT name FROM sqlite_master WHERE type='table' AND name='purchases_articles'"
        new_version = cur.execute(sql).fetchone()
        if not new_version:
            response['message'] = 'Data set is void or incomplete, please run the crawler script.\n'
            if print_info:
                print(response['message'] + '\n')
                exit(0)
            else:
                return response
        else:
            global occurrence_per_item, total_transactions, articles
            occurrence_per_item = {}
            total_transactions_db = cur.execute("SELECT COUNT(nr) FROM (SELECT purchase_id AS nr "
                                                "FROM purchases_articles GROUP BY purchase_id)").fetchone()

            cur.execute("SELECT ingredient_id, ingredient_name FROM ingredient_names")
            # articles = dict(cur.fetchall())
            # @todo: change me with the above line before handover?
            for article in cur.fetchall():
                articles[article[0]] = '{} [{}]'.format(article[1].replace("'", "`"), article[0])

            sql_p = "SELECT purchase_id, article_id FROM purchases_articles WHERE purchase_id < {} " \
                    "ORDER BY purchase_id".format(int(transactions_nr)+1)
            cur.execute(sql_p)
            results = cur.fetchall()

            # print(results)
            if len(results) == 0:
                response['message'] = 'Data set is void or incomplete, please run the create_dataset script.\n'
                if print_info:
                    print(response['message'] + '\n')
                    exit(0)
                else:
                    return response

            crt_purchase = 1
            crt_purchase_list = []
            for purchase_db in results:
                if crt_purchase == purchase_db[0]:
                    # filter our items occurring more than once
                    if purchase_db[1] not in crt_purchase_list:
                        crt_purchase_list.append(purchase_db[1])
                else:
                    # ending crt transaction, starting with the next one
                    purchases_list[crt_purchase] = crt_purchase_list
                    crt_purchase = purchase_db[0]
                    crt_purchase_list = [purchase_db[1]]

            purchases_list[crt_purchase] = crt_purchase_list
            total_transactions = len(purchases_list)

            set_occurrences_per_item(purchases_list)

            response['error'] = False
            if not print_info:
                return {**response, **{'articles': articles, 'purchases_list': purchases_list,
                                       'tot_transactions_db': total_transactions_db[0]}}
                # 'occurences_item': occurrence_per_item'
    con.close()
    return response


# If k_value is set, apply the k-anonymity value.
def anonymize_items(k_value, naive_method=True):
    global purchases_list, fname_method, set_k_value, total_transactions
    set_k_value = k_value
    if purchases_list and len(purchases_list) > 0:
        if naive_method:
            k_anon_naive(k_value)
        else:
            k_anon_similar2(k_value, False)
            fname_method = 'kG2'

        total_transactions = len(k_purchases_list)
        set_occurrences_per_item(k_purchases_list)

        if debug and debug_lvl == 3:
            transactions_to_file(purchases_list, '_transactions_' + str(k_value) + '.log')
            transactions_to_file(k_purchases_list, '_transactions_' + str(k_value) + '_k.log')

        return k_purchases_list
    else:
        print('anonymize_items() :: purchases list was empty')
        return None


# go over all purchases, group them using modulo
def k_anon_naive(k_value):
    global k_purchases_list
    group_id = 1
    group_elem = 0
    k_purchases_list = {group_id: []}

    for purchase in purchases_list:
        group_elem += 1
        for item in purchases_list[purchase]:
            if item not in k_purchases_list[group_id]:
                k_purchases_list[group_id].append(item)
        if purchase % k_value == 0:
            group_id += 1
            group_elem = 0
            k_purchases_list[group_id] = []  # purchases_list[purchase][:]

    if group_elem > 0:
        k_purchases_list[len(k_purchases_list) - 1].extend(k_purchases_list.pop(len(k_purchases_list), None))

    return k_purchases_list


# go over all purchases, group them using difflib ratio
def k_anon_similar(k_value, v_similar=False):
    import difflib
    global k_purchases_list
    # _print_dict(purchases_list)

    k_purchases_list = {}
    purchase_nr = 0
    similarities = {}
    max_ratio_per_transaction = []
    t_id_has_similar = []
    merging_lists = []
    # compare all transactions to all other and compute the similarity ratio
    for purchase in purchases_list:
        max_ratio = 0.0
        max_ratio_to = 0
        purchase_nr = purchase
        similarities[purchase] = []
        # order the previous similarities list by sm.ratio
        if purchase > 1:
            similarities[purchase - 1].sort(key=lambda x: x[1], reverse=True)

        for purchase_i in purchases_list:
            if purchase_i is purchase:
                continue
            sm = difflib.SequenceMatcher(None, purchases_list[purchase], purchases_list[purchase_i])
            if sm.ratio() > 0.0:
                t_id_has_similar.append(purchase_i)
            if sm.ratio() > max_ratio:
                max_ratio = sm.ratio()
                max_ratio_to = purchase_i
            similarities[purchase].append((purchase_i, sm.ratio()))
        if max_ratio_to > 0:
            max_ratio_per_transaction.append((max_ratio, max_ratio_to))

    # order the last similarities list by sm.ratio
    try:
        similarities[purchase_nr].sort(key=lambda x: x[1], reverse=True)
    except KeyError:
        debug_to_file('k_anon_similar() err for key {}'.format(purchase_nr))

    # _print_dict(similarities)
    # order the max_ratio_per _transaction list by sm.ratio from high to low
    max_ratio_per_transaction.sort(reverse=True)

    merged = []
    group_elem = 0

    for similar in max_ratio_per_transaction:
        line = ''
        if similar[1] not in merged:
            try:
                if merging_list_tid:
                    merging_lists.append(merging_list_tid)
            except (UnboundLocalError, NameError):
                pass
            merging_list_tid = []
            merged.append(similar[1])
            line += '{}: {} + '.format(len(k_purchases_list), similar[1])
            merging_list_tid.append(similar[1])
            for transaction in similarities[similar[1]]:
                if len(merged) == len(purchases_list) and group_elem == 0:
                    group_elem += 1
                    line += 'goes to UP^'
                    debug_to_file(line)
                    break
                # only add transactions which have a sm.ratio over 0.0 OR have 0.0 with all
                if transaction[0] not in merged and not v_similar or \
                        (v_similar and (transaction[1] > 0.0 or transaction[1] == 0.0 and not t_id_has_similar)):
                    if transaction[1] == 0.0:
                        debug_to_file('adding a null for {}'.format(transaction[0]))
                    line += '{}, '.format(transaction[0])
                    merging_list_tid.append(transaction[0])
                    merged.append(transaction[0])
                    group_elem += 1
                if group_elem == k_value-1:
                    group_elem = 0
                    break
    else:
        try:
            merging_lists.append(merging_list_tid)
        except UnboundLocalError:
            pass

    # add all not included transactions to a list
    not_incl_tid = []
    for t_id in purchases_list:
        if t_id not in merged:
            not_incl_tid.append(t_id)

    # if there are merging_lists shorter than k, use not_incl_tid to fill them
    c = 0
    while c < len(merging_lists):
        while (len(merging_lists[c]) < k_value) and len(not_incl_tid) > 0:
            merging_lists[c].append(not_incl_tid.pop())
        c += 1

    # if there are STILL not included transactions, add them to merging_lists
    if len(not_incl_tid) > 0:
        if len(not_incl_tid) >= k_value:
            merging_lists.append(not_incl_tid)
        else:
            merging_lists[len(merging_lists)-1].extend(not_incl_tid)

    # if there are STILL merging_lists shorter than k, join them to reach k_value
    short_l = []
    short_len = 0
    c = 0
    while c < len(merging_lists):
        if len(merging_lists[c]) < k_value:
            short_len += len(merging_lists[c])
            short_l.append(c)
        c += 1

    if short_len > 0:
        tmp_list = []
        short_l.sort(reverse=True)
        for l in short_l:
            try:
                tmp_list.extend(merging_lists.pop(l))
            except IndexError:
                debug_to_file('IndexError while merging_lists.pop(l) for {}'.format(l))

        if short_len >= k_value:
            merging_lists.append(tmp_list)
        else:
            merging_lists[len(merging_lists)-1].extend(tmp_list)

    # print('\n\n\n{}'.format(merging_lists))

    for merge_list in merging_lists:
        k_transaction = []
        for t_id in merge_list:
            k_transaction.extend(purchases_list[t_id])
        k_purchases_list[len(k_purchases_list) + 1] = k_transaction

    # _print_dict(k_purchases_list)


# this is the modified version of the similarity grouping function.
# Intentionally kept separate even if the code is 98% similar.
def k_anon_similar2(k_value, v_similar=False):
    import difflib
    import statistics
    global k_purchases_list
    k_purchases_list = {}
    purchase_nr = 0
    similarities = {}
    max_ratio_per_transaction = []
    t_id_has_similar = []
    merging_lists = []
    all_ratios = []
    # compare all transactions to all other and compute the similarity ratio
    for purchase in purchases_list:
        max_ratio = 0.0
        max_ratio_to = 0
        purchase_nr = purchase
        similarities[purchase] = []
        # order the previous similarities list by sm.ratio
        if purchase > 1:
            similarities[purchase - 1].sort(key=lambda x: x[1], reverse=True)

        for purchase_i in purchases_list:
            if purchase_i is purchase:
                continue
            sm = difflib.SequenceMatcher(None, purchases_list[purchase], purchases_list[purchase_i])
            if sm.ratio() > 0.0:
                t_id_has_similar.append(purchase_i)
            if sm.ratio() > max_ratio:
                max_ratio = sm.ratio()
                max_ratio_to = purchase_i
            all_ratios.append(sm.ratio())
            similarities[purchase].append((purchase_i, sm.ratio()))
        if max_ratio_to > 0:
            max_ratio_per_transaction.append((max_ratio, max_ratio_to))

    # order the last similarities list by sm.ratio
    try:
        similarities[purchase_nr].sort(key=lambda x: x[1], reverse=True)
    except KeyError:
        debug_to_file('k_anon_similar() err for key {}'.format(purchase_nr))

    # _print_dict(similarities)
    # order the max_ratio_per _transaction list by sm.ratio from high to low
    max_ratio_per_transaction.sort(reverse=True)

    r_median = statistics.median(all_ratios)
    merged = []

    for similar in max_ratio_per_transaction:
        line = ''
        if similar[1] not in merged:
            try:
                if merging_list_tid:
                    merging_lists.append(merging_list_tid)
            except UnboundLocalError:
                pass
            merging_list_tid = []
            merged.append(similar[1])
            # line += '{}: {} + '.format(len(k_purchases_list), similar[1])
            merging_list_tid.append(similar[1])
            for transaction in similarities[similar[1]]:
                # print('{} v {} '.format(transaction[1], r_median))
                if transaction[1] > r_median and transaction[0] not in merged:
                    line += '{}, '.format(transaction[0])
                    # out_debug('{}'.format(similarities[transaction[0]]))
                    merging_list_tid.append(transaction[0])
                    merged.append(transaction[0])

            # print(line+'\n')
    else:
        try:
            merging_lists.append(merging_list_tid)
        except (UnboundLocalError, NameError):
            pass

    # add all not included transactions to a list
    not_incl_tid = []
    for t_id in purchases_list:
        if t_id not in merged:
            not_incl_tid.append(t_id)

    # if there are merging_lists shorter than k, use not_incl_tid to fill them
    c = 0
    while c < len(merging_lists):
        while (len(merging_lists[c]) < k_value) and len(not_incl_tid) > 0:
            merging_lists[c].append(not_incl_tid.pop())
        c += 1

    # if there are STILL not included transactions, add them to merging_lists
    if len(not_incl_tid) > 0:
        if len(not_incl_tid) >= k_value:
            merging_lists.append(not_incl_tid)
        else:
            merging_lists[len(merging_lists)-1].extend(not_incl_tid)

    # if there are STILL merging_lists shorter than k, join them to reach k_value
    short_l = []
    short_len = 0
    c = 0
    while c < len(merging_lists):
        if len(merging_lists[c]) < k_value:
            short_len += len(merging_lists[c])
            short_l.append(c)
        c += 1

    if short_len > 0:
        tmp_list = []
        short_l.sort(reverse=True)
        for l in short_l:
            try:
                tmp_list.extend(merging_lists.pop(l))
            except IndexError:
                debug_to_file('IndexError while merging_lists.pop(l) for {}'.format(l))

        if short_len >= k_value:
            merging_lists.append(tmp_list)
        else:
            merging_lists[len(merging_lists)-1].extend(tmp_list)

    # print('\n\n\n{}'.format(merging_lists))
    line = '(p:{}, s:{}, max_r:{}). k: {} => len: '.format(len(purchases_list), min_support, max_ratio, k_value)
    for merge_list in merging_lists:
        line += '{}, '.format(len(merge_list))
        k_transaction = []
        for t_id in merge_list:
            k_transaction.extend(purchases_list[t_id])
        k_purchases_list[len(k_purchases_list) + 1] = k_transaction

    # print(line)


# filter items which have support < min_support
def filter_items(min_support_in, occurrences_saved=None, use_k=False):
    global total_transactions, min_support_factor, occurrence_per_item, min_support, message, print_info
    if not use_k:
        total_transactions = len(purchases_list)
    itemSupport.clean()
    if len(occurrence_per_item) == 0:
        if occurrences_saved and len(occurrences_saved) > 0:
            occurrence_per_item = occurrences_saved
        else:
            return {'message': 'something went wrong in filter_items()', 'error': True}

    try:
        min_support_in = int(min_support_in)
        if min_support_in is not None and 0 < min_support_in < 10:
            min_support = min_support_in
        elif min_support_in is not None and 10 < min_support_in <= 100:
            min_support = (min_support_in/10)
    except ValueError:
        if print_info:
            print('Invalid number entered, using default min_support: {}'.format(min_support_default))
            if min_support_in == 'x':
                exit()

    frac, min_support_factor = math.modf((total_transactions * min_support) / 10)
    if frac > .5:
        min_support_factor += 1
    print('min supp was: {}, min supp fact: {}, #tr: {}'.format(min_support, min_support_factor, total_transactions))

    for name, occurrences in list(occurrence_per_item.items()):
        if int(occurrences) >= min_support_factor:
            itemSupport.add(name, occurrences)

    itemSupport.sort()
    lost_percent = (len(occurrence_per_item) - len(itemSupport.all))/len(occurrence_per_item)*100
    lost_percent_pty = round(lost_percent, 2)
    message = 'lost {}% of items for min_supp: {} ( min_supp fact.: {}, #transactions: {} )'.format(
        lost_percent_pty, min_support, min_support_factor, total_transactions)

    out_debug('FINISHED STEP 1a: find L set. ')
    if print_info:
        print(itemSupport)
        print(message)
    else:
        return {'min_support_factor': min_support_factor, 'min_support': min_support, 'L': itemSupport.all,
                'message': message, 'lost_percent': lost_percent, 'total_transactions': total_transactions}

    # support_per_item_ord = sorted(occurrence_per_item.items(), key=lambda x: x[1], reverse=True)


def set_occurrences_per_item(p_list):
    global occurrence_per_item
    occurrence_per_item = {}
    # increase support_per_item:
    for purchase in p_list:
        for item in p_list[purchase]:
            item = int(item)
            if item in occurrence_per_item:
                occurrence_per_item[item] += 1
            else:
                occurrence_per_item[item] = 1


#
# find_assoc_rules():
# sort L in reverse order of support;
# for all items in supp reverse order:
#    3. calculate the cond pattern base:
#        use the hdr_tbl and go up the tree. Save all paths with the support
#    4. compute all conditional fp trees:
#       for each tree:
#           use the hdr_table and for each item:
#           IF min_supp > min_supp, go up the tree: save the branch as a set;
#           IF items have different min_support in the tree: split branch into different item sets;
#   5. FOR each item set and item extract the freq. patterns: <K,Y: 3> = K -> Y or Y -> K
#   6. check each association rule.
def find_assoc_rules(print_minimal=False, use_k=False, confidence=0.1, set_k=False):
    from core.fp_tree import FPTree
    global assoc_rules, assoc_rules_hr, purchases_list, k_purchases_list, conf_def, baseline_rules, in_baseline_rules, \
        in_baseline_rules_inv, fname_method
    if confidence > 0.1:
        conf_def = confidence
    if use_k and set_k_value > 1:
        purchases_tmp = copy.deepcopy(k_purchases_list)
    else:
        purchases_tmp = copy.deepcopy(purchases_list)
    tree = FPTree()
    transactions = {}
    assoc_rules = []
    assoc_rules_hr = []
    processes = []
    in_baseline_rules = None
    in_baseline_rules_inv = None
    response = {'purchases_min_supp': transactions, 'assoc_rules': assoc_rules, 'assoc_rules_hr': assoc_rules_hr,
                'freq_patterns': {}}
    if set_k_value > 1:
        in_baseline_rules = []
        in_baseline_rules_inv = []
        response['in_base'] = in_baseline_rules
        response['in_base_inv'] = in_baseline_rules_inv
    for transaction in purchases_tmp:
        row = 't[{}] {} -> '.format(transaction, purchases_tmp[transaction])
        a_list = []
        # for each transaction only keep items with supp > min_supp ordered by support DESC
        for item in itemSupport.all:
            if item[0] in purchases_tmp[transaction]:
                a_list.append(item[0])
        if len(a_list) > 0:
            transactions[transaction] = a_list
            tree.insert(a_list)
            row += colors.colorize('{}'.format(a_list))
        if print_info:
            util.print(row)

    if print_info:
        print('{} out of {}'.format(len(transactions), len(purchases_tmp)))
        tree.draw(print_minimal)
    else:
        response['purchases_min_supp'] = transactions
        response['FPTree'] = tree.return_all_nodes()
    # if L size is greater than max_L_size, only print/return the purchases with min supp and FP tree
    if len(itemSupport.all) > max_L_size:
        msg = 'L set size {} is greater than {} allowed by max_L_size. Change it in {}.'.\
            format(len(itemSupport.all), max_L_size, __file__)
        if print_info:
            print(msg)
            return False
        else:
            response['message'] = msg
            return response


    out_debug('FINISHED STEP 1b: apply set L to transactions ')
    out_debug('FINISHED STEP 2: create FP Tree ')

    # calculate the cond pattern base:
    cond_patt_base = {}
    # gen_cond_patt_base(cond_patt_base, tree, colors)
    itemSupport.sort(False)
    for item in itemSupport.all:
        links_pre = colors.colorize('v: {}'.format(item[0]), colors.WARN)
        links = ''
        if item[0] in tree.hdr_table:
            node_id = tree.hdr_table[item[0]]
            # a_list = []
            # cond_patt_base[item[0]] = []
            links += ' -> nid: {}'.format(node_id)
            while node_id is not 0:
                node = tree.get_node_by_nid(node_id)
                support = node.support
                a_list = tree.get_path_up(node, 'val')
                if print_info:
                    util.print('from nid: {} path: {}'.format(node_id, a_list))

                node_id = node.next_node_id

                links += ' -> nid: {}'.format(node_id)
                if len(a_list) > 0:
                    if cond_patt_base.get(item[0]) is None:
                        cond_patt_base[item[0]] = []
                    a_list.reverse()
                    cond_patt_base[item[0]].append({support: a_list})

        if print_info or debug:
            util.print(colors.colorize(links_pre + links, colors.WARN))
            print('+ {} pattern base is: {}'.format(links_pre, cond_patt_base))

    out_debug('FINISHED STEP 2: extract pattern base. ')
    if use_k and set_k_value > 1:
        baseline_rules = load_baseline_file()

    debug_to_file('len_b_rules: {}, b_rules: {}, k_value: {}'.format(len(baseline_rules), baseline_rules, set_k_value))

    if len(cond_patt_base) > 0:
        # global freq_patterns
        out_debug('imported cond FP-tree')
        freq_patterns_tmp = []
        l_ar_hr = None
        l_ar_inb = None
        l_ar_inb_inv = None
        if multi_proc_mode and True:
            manager = Manager()
            l_ar_hr = manager.list()
            l_ar_inb = manager.list()
            l_ar_inb_inv = manager.list()

        for item in cond_patt_base:
            if multi_proc_mode:
                p = Process(target=cp_tree_and_freq_patterns,
                            args=(cond_patt_base, item, freq_patterns_tmp, l_ar_hr, l_ar_inb, l_ar_inb_inv))
                processes.append(p)
                p.start()
                # p.join()
            else:
                cp_tree_and_freq_patterns(cond_patt_base, item, freq_patterns_tmp)

        out_debug('==== LAST STEP: rules output & save ====')
        if len(processes) > 0:
            for proc in processes:
                proc.join()

        if not assoc_rules_hr:
            if l_ar_hr is not None:
                assoc_rules_hr = list(l_ar_hr)
                out_debug('Manager list was NOT NONE. len l_ar: {}, len assoc_rules {}'.
                          format(len(l_ar_hr), len(assoc_rules_hr)))
        if multi_proc_mode and l_ar_inb is not None and in_baseline_rules is not None:
            # l_list(l_ar_inb)
            for rule in l_ar_inb:
                if rule not in in_baseline_rules:
                    in_baseline_rules.append(rule)
        if multi_proc_mode and l_ar_inb_inv is not None and in_baseline_rules_inv is not None:
            for rule in l_ar_inb_inv:
                if rule not in in_baseline_rules_inv:
                    in_baseline_rules_inv.append(rule)

        if not print_info:
            if freq_patterns_tmp:
                response['freq_patterns'] = freq_patterns_tmp

            response['assoc_rules_hr'] = assoc_rules_hr
            if in_baseline_rules is not None:
                response['in_base'] = in_baseline_rules
            if in_baseline_rules_inv is not None:
                response['in_base_inv'] = in_baseline_rules_inv
            response.pop('assoc_rules', None)

        else:
            print('{}\nAssociation rules:\n{}'.format('=' * 40, '=' * 40))
            for rule in assoc_rules_hr:
                inverted = ''
                if rule[2]:
                    inverted = 'inv. direction due to higher conf.'

                print('{} -> {}, c: {} {}'.format(rule[0], rule[1], rule[3],
                                                  colors.colorize(inverted, colors.WARN)))
            if in_baseline_rules:
                print('from which {} were in the baseline'.format(len(in_baseline_rules)))
        # if this is a k anon step, and k=1 save rules for baseline comp.
        if set_k and set_k_value == 1:
            save_baseline_file()

        # save results
        in_base = None
        tot_base = None
        if in_baseline_rules:
            in_base = len(in_baseline_rules)
        if baseline_rules:
            tot_base = len(baseline_rules)
        results_to_json('results_{}_m{}_k{}_c{}_{}.json'.
                        format(fname_method, min_support, set_k_value, conf_def, total_transactions), assoc_rules_hr,
                        itemSupport.all, total_transactions, in_base, tot_base)

    return response


# global assoc_rules, purchases_list, k_purchases_list, conf_def
def cp_tree_and_freq_patterns(cond_patt_base, item, freq_patterns_tmp, l_ar=None, l_ar_inb=None, l_ar_inb_inv=None):
    # skipping tree generation for 1 len items
    if len(cond_patt_base[item]) == 1 and len(cond_patt_base[item][0]) == 1:
        support, value = cond_patt_base[item][0].popitem()
        if support >= min_support_factor:
            # value.append(item)
            freq_patterns_tmp.append({support: value + [item]})
    else:
        from core.cond_fp_tree import CFPTree
        # generate cond FP tree
        cp_tree = CFPTree()
        for transaction in cond_patt_base[item]:
            for support, val in transaction.items():
                # print('{} -> {}'.format(k, v))
                cp_tree.insert(val, support)
        if print_info or debug_lvl > 1:
            cp_tree.draw()
        freq_patterns_i = (cp_tree.get_all_paths(min_support_factor, item))  # @todo: return infreq_vals?
        out_debug('freq_patt for {}: {}'.format(item, freq_patterns_i))
        if len(freq_patterns_i) > 0:
            freq_patterns_tmp.append(freq_patterns_i)

    out_debug('FINISHED STEP 3: find freq_patterns')

    find_and_check_patt(freq_patterns_tmp, l_ar, l_ar_inb, l_ar_inb_inv)
    # print('q was {}'.format(queue.qsize()))
    out_debug('FINISHED STEP 4: freq_patterns => assoc_rules')


# find_assoc_rules helper methods
def find_and_check_patt(freq_patterns, l_ar, l_ar_inb, l_ar_inb_inv):
    global print_info
    out_debug('all freq_patt: {} \n'.format(freq_patterns), highlight=True)
    out_debug('_r power set calc starts here')
    for item in freq_patterns:
        try:
            find_and_check_patt_r(item, l_ar, l_ar_inb, l_ar_inb_inv)
        except NameError as err:
            debug_to_file('find_and_check_patt(): {}'.format(err))


def find_and_check_patt_r(a_dict, l_ar, l_ar_inb, l_ar_inb_inv):
    if multi_proc_mode:
        print('parent process: {}, pid: {}'.format(getppid(), str(getpid())))
    try:
        # item is a dict, ex: {24: [26, 5]}
        all_items = next(iter(a_dict.values()))
        item = all_items[0]
        power_set = util.fp_powerset(all_items[1:], 1, 4)
        for comb in power_set:
            # rules {1..n} -> i plus inverse
            get_rule_confidence(comb, item, l_ar, l_ar_inb, l_ar_inb_inv)
            # print('looking for {}, comb {}'.format(item, comb))
    except AttributeError:
        # for lists of dict, ex: [{25: [56, 6, 5], 30: [56, 5]}, {30: [56, 5]}, {26: [56, 22]}]
        print('got AttributeError on {}, type {}'.format(a_dict, type(a_dict)))
        try:
            for item in a_dict:
                find_and_check_patt_r(item, l_ar, l_ar_inb, l_ar_inb_inv)
        except TypeError:
            # print('got TypeError on {}, type {}'.format(a_dict, type(a_dict)))
            debug_to_file('got TypeError on {}, type {}'.format(a_dict, type(a_dict)))
            pass


def get_rule_confidence(itemset, item, l_ar, l_ar_inb, l_ar_inb_inv):
    global print_info, assoc_rules, assoc_rules_hr, basic_mode
    if len(itemset) == 1 and item in itemset:
        return
    better_inv = False
    supp_left = get_support(itemset)
    supp_all = get_support(itemset, [item])
    supp_right = get_support([item])
    if not basic_mode:
        conf = 0
        conf_inv = 0
        if supp_left > 0:
            conf = (supp_all / supp_left)
        if supp_right > 0:
            conf_inv = (supp_all / supp_right)

        if conf < conf_def and conf_inv < conf_def:
            return False

        lift = supp_all / (supp_left * supp_right)
        if conf_inv > conf:
            better_inv = True
            if conf_inv == 1:
                conv = 9999
            else:
                conv = (1 - supp_right) / (1 - conf_inv)
            left = item
            right = itemset
            r_conf = conf_inv
        else:
            if conf == 1:
                conv = 9999
            else:
                conv = (1 - supp_left) / (1 - conf)
            left = itemset
            right = item
            r_conf = conf
    else:
        left = itemset
        right = item
        r_conf = supp_all
        lift = supp_left
        conv = supp_right

    left_hr = id_to_names(left)
    right_hr = id_to_names(right)

    rule = (left, right, round(r_conf, 2), better_inv, round(lift, 3), round(conv, 3))
    rule_hr = (left_hr, right_hr, rule[2], rule[4], rule[5], rule[3])

    if set_k_value > 1:
        if baseline_rules:
            if left_hr + '>' + right_hr in baseline_rules:
                l_ar_inb.append(rule_hr)
            elif left_hr + '<' + right_hr in baseline_rules:
                l_ar_inb_inv.append(rule_hr)

    tot_found = 0
    '''
    if multi_proc_mode:
        if l_ar is not None:
            tot_found = len(l_ar)
    else:
        tot_found = len(assoc_rules)
    '''
    out_debug('found rule: {}. {}, min_conf :{}, s_a:{}, s_l:{}, s_l_i:{}'.format(
        tot_found, rule, conf_def, supp_all, supp_left, supp_right))

    # IF multi_proc mode add the rule to the queue, print rule and queue size.
    # ELSE print just the rule:
    if rule not in assoc_rules:
        assoc_rules.append(rule)
        assoc_rules_hr.append(rule_hr)
    if l_ar is not None and rule_hr not in l_ar:
        l_ar.append(rule_hr)


def get_support(itemset, item=None):
    if item:
        set_items = set(itemset) | set(item)
    else:
        set_items = set(itemset)
    occurrence = 0
    for purchase in purchases_list:
        if set_items.issubset(set(purchases_list[purchase])):
            occurrence += 1
    return occurrence / len(purchases_list)


def id_to_names(itemset):
    global articles
    try:
        return articles[itemset]
    except KeyError:
        return ", ".join(articles[item] for item in itemset)
    except:
        debug_to_file('id_to_names() died on :{}'.format(itemset))


def save_baseline_file():
    global assoc_rules_hr
    with open('baseline.log', 'w+') as f:
        for rule in assoc_rules_hr:
            f.write("%s\n" % '{}>{}'.format(rule[0], rule[1]))


def load_baseline_file():
    with open('baseline.log', 'r') as f:
        return f.read().splitlines()


# helper methods
def _print_dict(d, key=None):
    if d:
        if key:
            print('{}. {} ({})\n'.format(key, d[key], type(d[key])))
        else:
            for item in d:
                print('{}. {} ({})\n'.format(item, d[item], type(d[item])))


def _print_k_cmp(key=None):
    global purchases_list, k_purchases_list
    if key:
        print('\n{}. {}'.format(key, purchases_list[key]))
        if key in k_purchases_list:
            print('{}. {}'.format(colors.colorize(str(key)), k_purchases_list[key]))
    else:
        for purchase in purchases_list:
            print('\n{}. {}'.format(purchase, purchases_list[purchase]))
            if purchase in k_purchases_list:
                print('{}. {}'.format(colors.colorize(str(purchase)), k_purchases_list[purchase]))


def _print_q(queue):
    lo_q = queue
    while not lo_q.empty():
        print('item was: {}'.format(lo_q.get()))


def out_debug(msg, min_level=None, highlight=False):
    if (debug_lvl == 2 and print_info) or not debug:
        return
    color = colors.LIBL
    if min_level and int(debug_lvl) < int(min_level):
        return
    if highlight:
        msg = '{} {}'.format(msg, '=+=' * 33)
        color = colors.HDR
    if debug or debug_lvl > 3:
        print(colors.colorize(msg, color))


def debug_to_file(txt):
    with open('_error.log', 'a+') as f:
        f.write("%s\n" % txt + '\n')


def transactions_to_file(transactions, f_name):
    with open(f_name, 'w+') as f:
        for item in transactions:
            f.write("%s\n" % '{}. {}'.format(item, transactions[item]))


def results_to_json(f_name, ar, L, nr_transactions, nr_in_base=None, nr_base=None):
    import json
    import zipfile
    if not os.path.exists(results_dir):
        os.makedirs(results_dir)
    with open(results_dir + f_name, 'w+') as f:
        f.write("%s" % '{\n')
        f.write("%s\n" % '"nr_transact_min_supp":{}, '.format(nr_transactions))
        if nr_in_base:
            f.write("%s\n" % '"in_baseline_rules":{}, '.format(nr_in_base))
        if nr_base:
            f.write("%s\n" % '"from_baseline_rules":{}, '.format(nr_base))
        f.write("%s" % '"L":')
        json.dump(L, f)
        f.write("%s" % ',\n"ar":')
        json.dump(ar, f)
        f.write("\n%s" % '}')

    if compress_results:
        try:
            import zlib
            compression = zipfile.ZIP_DEFLATED
        except (ImportError, ModuleNotFoundError) as e:
            compression = zipfile.ZIP_STORED

        if os.path.isfile(results_dir + f_name):
            with zipfile.ZipFile(results_dir + f_name + '.zip', 'w') as myzip:
                myzip.write(results_dir + f_name, compress_type=compression)

