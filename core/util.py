class Util:
    def __init__(self, debug=False):
        self.debug = debug

    def print(self, text):
        if self.debug:
            print(text)

    @staticmethod
    def print_list(a_list):
        print("list w/ len {}:".format(len(a_list)))
        for k, v in a_list.items():
            print("{} -> {} ".format(k, v))

    @staticmethod
    def fp_powerset(a_list, min_elem_nr=1, max_elem_nr=None):
        from itertools import chain, permutations, combinations
        s = list(a_list)
        if not min_elem_nr:
            min_elem_nr = 1
        if not max_elem_nr:
            max_elem_nr = len(s) + 1
        return chain.from_iterable(combinations(s, r) for r in range(min_elem_nr, max_elem_nr))


class BColors:
    HDR = '\033[95m'
    BL = '\033[34m'
    CYAN = '\033[96m'
    GR = '\033[92m'
    WARN = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    LIBL = '\033[94m'

    def colorize(self, text=None, color=None):
        if not color:
            color = self.CYAN
        text = color + text + self.ENDC
        return text
