import os.path
import sys
import sqlite3 as lite
import statistics

path = os.path.join(os.path.abspath('.'), __file__)
path_cwd = os.getcwd()

prefix_path_sql = ''

if 'core' in path:
    prefix_path_sql = '../data'

sql_file_name = 'chefkoch.w.data'
first_dataset_nr = 0
last_dataset_nr = 20
sum_avg = 0
sum_avg_dist = 0
p_avgs = []
p_avgs_dist = []

while first_dataset_nr < last_dataset_nr:
    crt_sql_file = '{}/{}{}.db'.format(prefix_path_sql, sql_file_name, first_dataset_nr)
    first_dataset_nr += 1
    con = lite.connect(crt_sql_file)
    con.text_factory = str
    sql_min = "SELECT min(nr) FROM (SELECT count(*) AS nr FROM purchase_articles GROUP BY purchase_id)"
    sql_max = "SELECT max(nr) FROM (SELECT count(*) AS nr FROM purchase_articles GROUP BY purchase_id)"
    sql_avg = "SELECT avg(nr) FROM (SELECT count(*) AS nr FROM purchase_articles GROUP BY purchase_id)"
    sql_avg_dist = "SELECT avg(count) FROM " \
                   "(SELECT count(distinct(article_id)) AS count FROM purchase_articles GROUP BY purchase_id);"
    cur = con.cursor()
    cur.execute(sql_min)
    p_min = cur.fetchone()[0]
    cur.execute(sql_max)
    p_max = cur.fetchone()[0]
    cur.execute(sql_avg)
    p_avg = cur.fetchone()[0]
    p_avgs.append(p_avg)
    cur.execute(sql_avg_dist)
    p_avg_dist = cur.fetchone()[0]
    p_avgs_dist.append(p_avg_dist)

    print('\\boxplot{{{}}}{{{}}}{{{}}}{{{}}}{{{}}}'.format(first_dataset_nr, p_avg, p_min, p_max,
                                                           p_avg_dist))
    sum_avg += p_avg
    sum_avg_dist += p_avg_dist
    con.close()

print('avg: {}, {}, sd: {}'.format(sum_avg, sum_avg/first_dataset_nr, statistics.stdev(p_avgs)))
print('avg_dist: {}, {}, sd: {}'.format(sum_avg_dist, sum_avg_dist/first_dataset_nr, statistics.stdev(p_avgs_dist)))
