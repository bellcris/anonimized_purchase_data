class Node:
    """
    """

    def __init__(self, nid=0, value=None, support=0, level=0, children=None):
        self.nid = nid
        self.value = value
        self.support = support
        self.parent = None
        if children is None:
            self.__children = []
        else:
            self.__children = children
        self.next_node_id = 0
        self.level = level
        self.print_minimal = False

    '''
    increases the counter [support] propriety of the node
    '''
    def increase_counter(self):
        self.support += 1

    '''
    adds a child node to a node
    '''
    def add_child(self, node):
        if isinstance(node, Node) or node is None:
            self.__children.append(node)

    '''
    sets the parent propriety of a node
    '''
    def add_parent(self, nid):
        if nid is not None:
            self.parent = nid

    '''
    :return []
    returns all children of a node
    '''
    def get_children(self):
        return self.__children

    def to_dict(self):
        return {'id': self.nid, 'val': self.value, 'supp': self.support, 'p': self.parent, 'c': self.level,
                'lvl': self.level, 'nxt': self.next_node_id}

    def __repr__(self):
        if self.print_minimal:
            return "{}.[{}|{} p:{} -->{}]".format(self.nid, self.value, self.support, self.parent, self.next_node_id)
        return "{}.[{}|{} p:{} #cld:{} LVL:{} nxt_lnk:{}]".format(self.nid, self.value, self.support, self.parent,
                                                                  len(self.__children), self.level, self.next_node_id)
