from core.fp_tree import FPTree


class CFPTree(FPTree):

    def __init__(self):
        FPTree.__init__(self)
        self.end_tbl = []

    '''
    overrides the parent.add_branch() method 
    also adding the end node nid to the end table
    '''
    def add_branch(self, transaction, node, level=1, counter=1):
        new_node = FPTree.add_branch(self, transaction, node, level, counter)
        if new_node:
            self.end_tbl.append(new_node.nid)

    '''
    overrides the parent.draw() 
    also printing the end table
    '''
    def draw(self, minimal=False):
        print(self.colors.colorize('\ncond FP Tree:', self.colors.UNDERLINE))
        FPTree.draw(self)
        self.util.print(self.colors.colorize('end_tbl: {0}\n'.format(self.end_tbl)))

    '''
    :return []
    retrieves all pats from the cond FP tree. Adds all passing paths [support>min_supp] to the list of freq_patterns
    '''
    def get_all_paths(self, min_support, item):
        freq = []
        infreq_nodes = []
        infreq_values = {}
        for entry in self.end_tbl:
            node = self.get_node_by_nid(entry)
            if len(node.get_children()) > 0:
                continue
            freq_branch = dict()

            while node.parent:
                self.util.print('{} |{}'.format(node.value, node.support))
                if node.support >= min_support:
                    if node.support not in freq_branch.keys():
                        freq_branch[node.support] = [item]
                    for key in freq_branch.keys():
                        freq_branch[key].append(node.value)
                # if node support is less than min_supp add the value to a temp dict. Check all items at the end, maybe
                # added support from different branches is >= than min_support
                else:
                    if node.nid not in infreq_nodes:
                        if node.value not in infreq_values.keys():
                            infreq_values[node.value] = node.support
                        else:
                            infreq_values[node.value] += node.support
                        infreq_nodes.append(node.nid)
                node = self.get_node_by_nid(node.parent)
            if len(freq_branch) > 0 and freq_branch not in freq:
                freq.append(freq_branch)
        self.util.print('CFPTree.get_all_paths(infreq_vals): {}'.format(infreq_values))  #@todo: return this?
        # go over the items who didn't pass the min_support test, see if their sum does
        for val, item_freq in infreq_values.items():
            if item_freq >= min_support:
                self.util.print('adding infreq_items {}:[{}, {}]'.format(item_freq, item, val))
                freq.append({item_freq: [item, val]})

        if len(freq) == 1:
            return freq[0]
        return freq

