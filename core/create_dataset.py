import os.path
import sqlite3 as lite
from random import randrange

path = os.path.join(os.path.abspath('.'), __file__)
path_cwd = os.getcwd()
prefix_path_sql = ''
sql_file_def = 'chefkoch.w.data.db'
create_multi = 20

if 'core' in path:
    from util import BColors
    from util_func import print_progress
else:
    from core.util import BColors
    from core.util_func import print_progress

if 'core' in path_cwd:
    prefix_path_sql = '../'
'''
menu ->
    re(C)reate tables
    generate (s)hops
        generate name
    generate (c)ustomers
    generate (p)urchases
        select from recipes, ingredients, customers, shops
        generate new row
        insert data
    (re)generate all
        re(C)reate tables
        generate (s)hops
        generate (c)ustomers
        generate (p)urchases        

Data set structure:
shops = number_of_shops(def:25) x {shop_id, shop_name, zip_code(1..100)}
customers = number_of_customers(def:330) x 
            {user_id, user_name, user_surname, user_gender(F|M), user_dob, zip_code(1..100)}  
purchases = number_of_purchases(def:500) x 
            {purchased_id, user_id(rand), shop_id(rand), purchased_on, recipe(rand)(0..3) + ingredients(rand)(1..9)}
'''

known_recipes = []
known_ingredients = []

number_of_ingredients = 0
number_of_recipes = 0
number_of_customers = 0
number_of_shops = 0
number_of_purchases = 0
customer_target_nr = 330
shops_target_nr = 25
purchases_target_nr = 500
min_recipes_purchase = 0
max_recipes_purchase = 3
min_ingredients_purchase = 1
max_ingredients_purchase = 9
min_zip_code = 1
max_zip_code = 100
names = ['Black', 'Brown', 'Doe', 'Jones', 'Smith']
con_read = lite.connect(prefix_path_sql + '' + sql_file_def)
con_read.text_factory = str


def connect(sql_file):
    if not sql_file or sql_file == '':
        return None
    connection = lite.connect(prefix_path_sql + '' + sql_file)
    connection.text_factory = str
    return connection


def start():
    with con_read:
        cur = con_read.cursor()
        new_version = cur.execute("SELECT name FROM sqlite_master WHERE type='table' AND "
                                  "name='ingredient_names'").fetchone()

        if not new_version:
            print("There are no tables in the database, please run the crawler script")
            exit(0)


def create_tables(which=None, connector=None):
    if connector:
        this_con = connector
    else:
        this_con = con_read
    with this_con:
        # con.row_factory = lite.Row
        cur = this_con.cursor()
        if which is None or which is 'a':
            cur.execute("DROP table IF EXISTS articles")
            # (re)create the required tables
            cur.execute("CREATE table IF NOT EXISTS articles "
                        "(article_id INTEGER PRIMARY KEY,"
                        "article_type INTEGER,"
                        "article_price INTEGER,"
                        # "unit INTEGER,"
                        "article_name TEXT NOT NULL)")
        if which is None or which is 'c':
            cur.execute("DROP table IF EXISTS customers")
            cur.execute("CREATE table IF NOT EXISTS customers "
                        "(customer_id INTEGER PRIMARY KEY,"
                        "customer_name TEXT NOT NULL,"
                        "customer_surname TEXT NOT NULL,"
                        "customer_gender INTEGER,"
                        "customer_birthday DATE,"
                        "customer_zip_code INTEGER)")

        if which is None or which is 's':
            cur.execute("DROP table IF EXISTS shops")
            cur.execute("CREATE table IF NOT EXISTS shops "
                        "(shop_id INTEGER PRIMARY KEY,"
                        "shop_name TEXT NOT NULL,"
                        "shop_zip_code INTEGER)")

        if which is None or which is 'p':
            cur.execute("DROP table IF EXISTS purchase_articles")
            cur.execute("DROP table IF EXISTS purchases")
            cur.execute("CREATE table IF NOT EXISTS purchases "
                        "(purchase_id INTEGER PRIMARY KEY,"
                        "purchased_on datetime,"
                        "shop_id INTEGER,"
                        "customer_id INTEGER,"
                        # "purchase_method_of_payment INTEGER,"
                        "FOREIGN KEY(shop_id) REFERENCES shops(shop_id),"
                        "FOREIGN KEY(customer_id) REFERENCES customers(customer_id))")

            cur.execute("CREATE table IF NOT EXISTS purchases_articles "
                        "(article_id INTEGER,"
                        "purchase_id INTEGER,"
                        "FOREIGN KEY(article_id) REFERENCES article(article_id),"
                        "FOREIGN KEY(purchase_id) REFERENCES purchases(purchase_id))")

        this_con.commit()
        print("tables done!\n")


def gen_customers(connector=None):
    i = 1
    if connector:
        this_con = connector
    else:
        this_con = con_read
    cur = this_con.cursor()
    print_progress(0, customer_target_nr, prefix='Customers ({})'.format(customer_target_nr),
                   suffix='Complete', bar_length=50)
    while i < customer_target_nr+1:
        zip_code = my_randrange(min_zip_code, max_zip_code)
        dob = rnd_date(date_end='1/3/2001')
        gender = randrange(1, 3)
        surname = names[randrange(0, len(names))]
        if gender is 1:
            name = 'Jane '+str(i)
        else:
            name = 'John '+str(i)
        print_progress(i, customer_target_nr, prefix='Customers ({})'.format(customer_target_nr),
                       suffix='Complete', bar_length=50)
        cur.execute("INSERT INTO customers (customer_id, customer_name, customer_surname, customer_gender, "
                    "customer_birthday, customer_zip_code) VALUES (?, ?, ?, ?, ?, ?)",
                    (int(i), name, surname, gender, zip_code, dob))
        i = i + 1
    this_con.commit()
    print("Customers created!\n")


def gen_shops(connector=None):
    if connector:
        this_con = connector
    else:
        this_con = con_read
    i = 1
    cur = this_con.cursor()
    print_progress(0, shops_target_nr, prefix='Shops ({})'.format(shops_target_nr),
                   suffix='Complete', bar_length=50)
    while i < shops_target_nr+1:
        zip_code = my_randrange(min_zip_code, max_zip_code)
        print_progress(i, shops_target_nr, prefix='Shops ({})'.format(shops_target_nr),
                       suffix='Complete', bar_length=50)
        cur.execute("INSERT INTO shops (shop_id, shop_name, shop_zip_code) VALUES (?, ?, ?)",
                    (int(i), 'name' + str(i), zip_code))
        i = i + 1
    this_con.commit()
    print("Shops created!\n")


def gen_purchases(seed=None, connector=None):
    known_recipes = []
    known_ingredients = []

    with con_read:
        # con.row_factory = lite.Row
        cur = con_read.cursor()
        cur.execute("SELECT DISTINCT recipe_id FROM recipe_names")
        results = cur.fetchall()

        for recipe in results:
            known_recipes.append(recipe[0])

        cur.execute("SELECT DISTINCT ingredient_id FROM ingredient_names")
        results = cur.fetchall()

        for ingredient in results:
            known_ingredients.append(ingredient[0])

        if seed:
            import random
            random.seed(seed)
        if connector:
            cur_ins = connector.cursor()
            this_con = connector
            print('cur_ins is for connector {}'.format(connector))
        else:
            this_con = con_read
            cur_ins = cur
        print_progress(0, purchases_target_nr, prefix='Purchases ({})'.format(purchases_target_nr),
                       suffix='Complete', bar_length=50)
        crt_cnt = 1
        while crt_cnt < purchases_target_nr + 1:
            p_ingredients = []
            a = 0
            purchased_on = rnd_date(date_start='1/1/2010')
            # shop_id = my_randrange(1, shops_target_nr)
            customer_id = my_randrange(1, customer_target_nr)
            nr_recipes = my_randrange(min_recipes_purchase, max_recipes_purchase)
            nr_ingredients = my_randrange(min_ingredients_purchase, max_ingredients_purchase)
            while a < nr_recipes:
                p_ingredients = get_recipe_ingredients()
                a = a + 1
            a = 0
            # print("{} recipes w/ {} ingredients. Adding {} extr ingredients".format(nr_recipes, len(p_ingredients), nr_ingredients))
            while a < nr_ingredients:
                one_ingredient = my_randrange(1, len(known_ingredients))
                if one_ingredient not in p_ingredients:
                    p_ingredients.append(one_ingredient)
                    a = a + 1
            #
            print_progress(crt_cnt, purchases_target_nr, prefix='Purchases ({})'.format(purchases_target_nr),
                           suffix='Complete', bar_length=50)
            cur_ins.execute("INSERT INTO purchases (purchase_id, purchased_on, shop_id, customer_id) "
                            "VALUES (?, ?, ?, ?)", (int(crt_cnt), purchased_on, shops_target_nr, customer_id))
            for x in p_ingredients:
                cur_ins.execute(
                    "INSERT INTO purchases_articles (purchase_id, article_id) VALUES (?, ?)", (int(crt_cnt), x))
            crt_cnt = crt_cnt + 1
            this_con.commit()
        print("Purchases created!\n")


def get_recipe_ingredients(recipe_id=None):
    with con_read:
        # con.row_factory = lite.Row
        cur = con_read.cursor()
        if recipe_id:
            return cur.execute("SELECT ingredient_id FROM recipe_ingredients WHERE recipe_id=?",
                               (recipe_id,)).fetchall()
        else:
            ingredients = []
            results = cur.execute("SELECT ingredient_id FROM recipe_ingredients WHERE recipe_id IN "
                                  "(SELECT recipe_id FROM recipe_names ORDER BY RANDOM() LIMIT " +
                                  str(max_recipes_purchase) + ")").fetchall()
            for ingredient in results:
                ingredients.append(ingredient[0])
            return ingredients


def my_randrange(min_nr: int, max_nr: int):
    return randrange(min_nr, max_nr+1)


def rnd_date(date_start: str = None, date_end: str = None) -> str:
    import time
    import random
    date_format = '%m/%d/%Y'
    if not date_start:
        date_start = '1/1/1910'
    if not date_end:
        date_end = time.strftime(date_format)
    stime = time.mktime(time.strptime(date_start, date_format))
    etime = time.mktime(time.strptime(date_end, date_format))
    ptime = stime + random.random() * (etime - stime)

    return time.strftime(date_format, time.localtime(ptime))


colors = BColors()


def menu():
    choice = input("\ndo (" + colors.colorize('a', colors.FAIL) + ")ll, re/create (" + colors.colorize('t') + ")ables, "
                   + "gen (" + colors.colorize('c') + ")ustomers, gen (" + colors.colorize('p') + ")urchases, gen (" +
                   colors.colorize('s') + ")hops, e(" + colors.colorize('x', colors.WARN) + ")it: ")
    if choice == 'c':
        create_tables('c')
        gen_customers()
    elif choice == 'p':
        seed = input("please provide a seed; OR use default -system time or os.urandom: ")
        try:
            seed = int(seed)
        except ValueError:
            seed = None
            print('incorrect seed value provided, using default')
        create_tables('p')
        gen_purchases(seed)
    elif choice == 's':
        create_tables('s')
        gen_shops()
    elif choice == 't':
        create_tables()
    elif choice == 'a':
        create_tables()
        gen_shops()
        gen_customers()
        seed = input("please provide a seed; OR use default -system time or os.urandom: ")
        try:
            seed = int(seed)
        except ValueError:
            seed = None
            print('incorrect seed value provided, using default')
        gen_purchases()
        print("all done!")
    elif choice == 'x':
        exit()
    menu()


if create_multi:
    current_iter = 0
    while current_iter < create_multi:
        crt_file = 'chefkoch.w.data' + str(current_iter) + '.db'
        con_tmp = connect(crt_file)
        if con_tmp:
            print('file: {}, tmp_con: {}'.format(crt_file, con_tmp))
            create_tables(connector=con_tmp)
            gen_shops(connector=con_tmp)
            gen_customers(connector=con_tmp)
            gen_purchases(connector=con_tmp)
            con_tmp.close()
        current_iter += 1
else:
    start()
    menu()


con_read.close()
